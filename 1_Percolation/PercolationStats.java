import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

public class PercolationStats {

    private final int n;        // number of rows/columns in grid
    private final int trials;   // number of times percolation experiment will be run
    private double mean;
    private double stddev;

    // array tracking number of open sites when percolation achieved in ith
    // trial (ith entry = threshold for ith trial)
    private double[] thresholds;

    // perform independent trials on an n-by-n grid
    public PercolationStats(int n, int trials) {
        // check input
        if (n <= 0 || trials <= 0) {
            throw new IllegalArgumentException("n and trials must be greater than zero");
        }
        // assign initialization variables
        this.n = n;
        this.trials = trials;
        // initialize remaining members
        mean = -1;
        stddev = -1;
        thresholds = new double[trials];
        // run simulations
        simulate();
    }

    // sample mean of percolation threshold
    public double mean() {
        mean = StdStats.mean(thresholds);
        return mean;
    }

    // sample standard deviation of percolation threshold
    public double stddev() {
        stddev = StdStats.stddev(thresholds);
        return stddev;
    }

    // low endpoint of 95% confidence interval
    public double confidenceLo() {
        final double mean = (this.mean == -1) ? mean() : this.mean;
        final double stddev = (this.stddev == -1) ? stddev(): this.stddev;

        return mean - (1.96 * stddev) / Math.sqrt(trials);
    }

    // high endpoint of 95% confidence interval
    public double confidenceHi() {
        final double mean = (this.mean == -1) ? mean() : this.mean;
        final double stddev = (this.stddev == -1) ? stddev(): this.stddev;

        return mean + (1.96 * stddev) / Math.sqrt(trials);
    }

    private void simulate() {
        final int totalSites = n * n;    
        for (int t = 0; t < trials; t++) {
            // initialize percolation for current simulation
            final Percolation percolation = new Percolation(n);
            // randomly open sites until system percolates
            while (!percolation.percolates()) {
                // get random row and column indices
                final int row = StdRandom.uniform(1, n + 1);
                final int col = StdRandom.uniform(1, n + 1);
                // open site represented by randomly generated indices
                percolation.open(row, col);
            }
            // get number of open sites at end of simulation
            final double numberOpenSites = percolation.numberOfOpenSites();
            // record simulation threshold
            thresholds[t] = numberOpenSites / (totalSites);           
        }
    }

   // test client (see below)
    public static void main(String[] args) {
        final int n = Integer.parseInt(args[0]);
        final int T = Integer.parseInt(args[1]);
        // initialize clas for statistics generation
        final PercolationStats stats = new PercolationStats(n, T);
        // retrieve statistics
        final double mean = stats.mean();
        final double stddev = stats.stddev();
        final double confidenceLo = stats.confidenceLo();
        final double confidenceHi = stats.confidenceHi();
        // print them
        System.out.println("mean                    = " + mean);
        System.out.println("stddev                  = " + stddev);
        System.out.println("95% confidence interval = [" +
                           confidenceLo + ", " + confidenceHi + "]");
    }

}