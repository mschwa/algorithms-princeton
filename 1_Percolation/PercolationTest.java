import edu.princeton.cs.algs4.StdIn;

public class PercolationTest {
    public static void main(String[] args) {
        final int n = StdIn.readInt();
        final Percolation percolation = new Percolation(n);
        System.out.println("Percolation with " + n + " sites");
        while (!StdIn.isEmpty()) {
            final int row = StdIn.readInt();
            final int col = StdIn.readInt();
            System.out.println("\nOpening (" + row + "," + col + ")");
            percolation.open(row, col);
            System.out.println("Site added full? " + percolation.isFull(row, col));
            System.out.println("Number of open sites: " + percolation.numberOfOpenSites());
            System.out.println("System percoaltes? " + percolation.percolates());
        }
    }

}