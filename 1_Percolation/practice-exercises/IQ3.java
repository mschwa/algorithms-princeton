import java.util.Scanner;

/******************************************************************************
 *  Compilation:  javac WeightedQuickUnionUF.java
 *  Execution:  java WeightedQuickUnionUF < input.txt
 *  Dependencies: StdIn.java StdOut.java
 *  Data files:   https://algs4.cs.princeton.edu/15uf/tinyUF.txt
 *                https://algs4.cs.princeton.edu/15uf/mediumUF.txt
 *                https://algs4.cs.princeton.edu/15uf/largeUF.txt
 *
 *  Implemets a data structure that, given a set of n integers S = {0, 1, ...,
 *  n-1}, is able to remove an element from the set as well as find the successor
 *  of an element n in it, i.e. find the smallest x in S such that x >= n.
 * 
 *  Each element is initially regarded as a tree of size one rooted in its
 *  only element. Each tree keeps the maximum value contained in it as its
 *  canonical value.
 * 
 *  Using this model, the remove(x) operation is equivalent to performing
 *  union(x, x+1): both elements are merged into a tree, but we can hide
 *  the removed one by only retrieving the canonical element from the tree.
 *  
 *  With this implementation, to find the successor of x, we must simply
 *  retrieve the canonical element of the tree containing x+1.
 * 
 *  To treat the edge case where n-1 is removed, we maitain a sentinel element
 *  n that cannot be accessed by clients
 *
 ******************************************************************************/

/**
 *  The {@code WeightedQuickUnionUF} class represents a <em>union–find data type</em>
 *  (also known as the <em>disjoint-sets data type</em>).
 *  It supports the classic <em>union</em> and <em>find</em> operations,
 *  along with a <em>count</em> operation that returns the total number
 *  of sets.
 *  <p>
 *  The union-find data type models a collection of sets containing
 *  <em>n</em> elements, with each element in exactly one set.
 *  The elements are named 0 through <em>n</em>–1.
 *  Initially, there are <em>n</em> sets, with each element in its
 *  own set. The <em>cannonical elemement</em> of a set
 *  (also known as the <em>root</em>, <em>identifier</em>,
 *  <em>leader</em>, or <em>set representative</em>)
 *  is one distinguished element in the set. Here is a summary of
 *  the operations:
 *  <ul>
 *  <li><em>find</em>(<em>p</em>) returns the canonical element
 *      of the set containing <em>p</em>. The <em>find</em> operation
 *      returns the same value for two elements if and only if
 *      they are in the same set.
 *  <li><em>union</em>(<em>p</em>, <em>q</em>) merges the set
 *      containing element <em>p</em> with the set containing
 *      element <em>q</em>. That is, if <em>p</em> and <em>q</em>
 *      are in different sets, replace these two sets
 *      with a new set that is the union of the two.
 *  <li><em>count</em>() returns the number of sets.
 *  </ul>
 *  <p>
 *  The canonical element of a set can change only when the set
 *  itself changes during a call to <em>union</em>&mdash;it cannot
 *  change during a call to either <em>find</em> or <em>count</em>.
 *  <p>
 *  This implementation uses <em>weighted quick union by size</em>
 *  (without path compression).
 *  The constructor takes &Theta;(<em>n</em>), where <em>n</em>
 *  is the number of elements.
 *  The <em>union</em> and <em>find</em>
 *  operations  take &Theta;(log <em>n</em>) time in the worst
 *  case. The <em>count</em> operation takes &Theta;(1) time.
 *  <p>
 *  For alternative implementations of the same API, see
 *  {@link UF}, {@link QuickFindUF}, and {@link QuickUnionUF}.
 *  For additional documentation, see
 *  <a href="https://algs4.cs.princeton.edu/15uf">Section 1.5</a> of
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
public class IQ3 {
    private final int[] parent; // parent[i] = parent of i
    private final int[] size; // size[i] = number of elements in subtree rooted at i
    private int[] canonical; // canonical[i] = canonical value in tree containing i
    private boolean[] removed; // removed[i] = indicates whether i has been removed from set
    private int count; // number of components
    private int sentinel; // maitain reference to sentinel element
    /**
     * Initializes an empty union-find data structure with {@code n} elements
     * {@code 0} through {@code n-1}. Initially, each elements is in its own set.
     *
     * @param n the number of elements
     * @throws IllegalArgumentException if {@code n < 0}
     */
    public IQ3(final int n) {
        count = n;
        sentinel = n-1;
        parent = new int[n];
        size = new int[n];
        for (int i = 0; i <= n; i++) {
            parent[i] = i;
            size[i] = 1;
            canonical[i] = i; // reference to maximum element
            removed[i] = false;
        }
    }

    /**
     * Returns the number of sets.
     *
     * @return the number of sets (between {@code 1} and {@code n})
     */
    public int count() {
        return count;
    }
    
    /**
     * Returns the root of the set containing element {@code p}.
     *
     * @param p an element
     * @return the canonical element of the set containing {@code p}
     * @throws IllegalArgumentException unless {@code 0 <= p < n}
     */
    private int root(int p) {
        // find tree root, O(logN)
        while (p != parent[p]) {
            // compress path and follow it to find root.
            parent[p] = parent[parent[p]];
            p = parent[p];
        }
        return p;
    }

    /**
     * Returns the successor of element {@code p}. Special cases:
     * 
     *
     * @param p an element
     * @return the canonical element of the set containing {@code p}
     * @throws IllegalArgumentException unless {@code 0 <= p < n}
     */
    public int find(int p) {
        validate(p);
        // find tree root
        final int rootNext = root(p + 1);
        // get its canonical value
        final int canonical = this.canonical[rootNext];
        // handle edge case: if the retrieved cannonical value equals
        // the sentinel (greatest value) and p is a valid element,
        // it turns out that p is the successor of itself (highest
        // element still on the set)
        if (canonical == sentinel) {
            return this.canonical[p];
        } else {
            return canonical;
        }
        
    }

    /**
     * Returns true if the two elements are in the same set.
     * 
     * @param p one element
     * @param q the other element
     * @return {@code true} if {@code p} and {@code q} are in the same set;
     *         {@code false} otherwise
     * @throws IllegalArgumentException unless both {@code 0 <= p < n} and
     *                                  {@code 0 <= q < n}
     * @deprecated Replace with two calls to {@link #find(int)}.
     */
    @Deprecated
    public boolean connected(final int p, final int q) {
        return find(p) == find(q);
    }

    // validate that p is a valid index
    private void validate(final int p) {
        final int n = parent.length;
        if (p < 0 || p >= n) {
            throw new IllegalArgumentException("index " + p + " is not between 0 and " + (n - 1));
        }
        if (removed[p]) {
            throw new IllegalArgumentException(p + " has already been removed from set");
        }
    }

    /**
     * Merges the set containing element {@code p} with the the set containing
     * element {@code q}.
     *
     * @param p one element
     * @param q the other element
     * @throws IllegalArgumentException unless both {@code 0 <= p < n} and
     *                                  {@code 0 <= q < n}
     */
    private void union(final int p, final int q) {
        // get root values
        final int rootP = root(p);
        final int rootQ = root(q);
        if (rootP == rootQ)
            return;
        
        // get maximum canonical value associated with both roots
        final int cannonicalP = find(rootP);
        final int cannonicalQ = find(rootQ);
        final int max = Math.max(cannonicalP, cannonicalQ);

        // make smaller root point to larger one and associate maximum
        // canonical value for tree with root
        if (size[rootP] < size[rootQ]) {
            parent[rootP] = rootQ;
            size[rootQ] += size[rootP];
            canonical[rootQ] = max;
        } else {
            parent[rootQ] = rootP;
            size[rootP] += size[rootQ];
            canonical[rootP] = max;
        }
        count--;
    }

    public void remove(final int n) {
        // make sure n is valid and is still in set
        validate(n);
        // flag value as removed for validation
        removed[n] = true;
        // perform union
        union(n, n+1);
    }

    /**
     * Reads an integer {@code n} and a sequence of pairs of integers (between
     * {@code 0} and {@code n-1}) from standard input, where each integer in the
     * pair represents some element; if the elements are in different sets, merge
     * the two sets and print the pair to standard output.
     * 
     * @param args the command-line arguments
     */
    public static void main(final String[] args) {
        final Scanner in = new Scanner(System.in);
        final int n = in.nextInt();
        final IQ3 uf = new IQ3(n);
        while (!in.hasNext()) {
            final int p = in.nextInt();
            final int q = in.nextInt();
            if (uf.find(p) == uf.find(q)) continue;
            uf.union(p, q);
            System.out.println(p + " " + q);
        }
        in.close();
        System.out.println(uf.count() + " components");
    }

}
