import edu.princeton.cs.algs4.In;

public class BoggleTester {

    public static void main(String[] args) {
        In in = new In(args[0]);
        String[] dictionary = in.readAllStrings();
        BoggleSolver solver = new BoggleSolver(dictionary);

        int count = 0;
        long end = System.nanoTime() + 5000000000L;
        while (System.nanoTime() <= end) {
            final BoggleBoard board = new BoggleBoard();
            solver.getAllValidWords(board);
            count++;
        }

        System.out.println("Board solving rate: " + count / 5.0 + "board/s");

    }

}