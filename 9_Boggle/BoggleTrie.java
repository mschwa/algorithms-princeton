import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

public class BoggleTrie {

    private static final int R = 26;           // Radix of alphabet (A - Z)
    private static final char OFFSET = 'A';    // starting char of alphabet
    private int val;                           // val of trie node
    private final BoggleTrie[] next;           // subtries

    public BoggleTrie() {
        this.next = new BoggleTrie[R];
    }

    /**
     * @param letter: 
     * @return subtrie rooted at letter
     */
    public BoggleTrie step(char letter) {
        final int c = letter - OFFSET;
        return next[c];
    }

    /**
     * @return whether current trie is rooted at the
     *         end of a word
     */
    public boolean isEndofWord() {
        return val > 0;
    }

    /**
     * @param key
     * @return score of key considering all of it is
     *         contained in current trie
     */
    public int get(String key) {
        BoggleTrie x = get(this, key, 0);
        if (x == null) return 0;
        return x.val;
    }

    private BoggleTrie get(BoggleTrie x, String key, int d) {
        if (x == null) return null;
        if (d == key.length()) return x;
        int c = key.charAt(d) - OFFSET;
        return get(x.next[c], key, d + 1);
    }

    /**
     * 
     * @param key, key to insert in trie
     * @param val, value key maps to
     */
    public void put(String key, int val) {
        put(this, key, val, 0);
    }

    private BoggleTrie put(BoggleTrie x, String key, int val, int d) {
        if (x == null) x = new BoggleTrie();
        if (d == key.length()) {
            x.val = val;
        } else {
            int c = key.charAt(d) - OFFSET;
            x.next[c] = put(x.next[c], key, val, d + 1);
        }
        return x;
    }

    public static void main(String[] args) {
        In in = new In(args[0]);
        String[] dictionary = in.readAllStrings();
        BoggleSolver solver = new BoggleSolver(dictionary);
        BoggleBoard board = new BoggleBoard(args[1]);
        int score = 0;
        for (String word : solver.getAllValidWords(board)) {
            StdOut.println(word);
            score += solver.scoreOf(word);
        }
        StdOut.println("Score = " + score);
    }
}