/**
 * This could be made faster by modifying the BoggleTrie
 * to make it a private class within BoggleSolver, but for
 * educational purposes it was kept as a separate class
 */

import edu.princeton.cs.algs4.Bag;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.SET;

import java.util.ArrayList;
import java.util.List;

public class BoggleSolver {

    private final BoggleTrie trie;  // R-Way Trie with full dictionary
    private boolean[] visited;      // visited[i * m + j] = visited (i, j)?

    /**
     * Initializes the data structure using the given array of strings as the dictionary
     *
     * @param dictionary dictionary of words
     */
    public BoggleSolver(String[] dictionary) {
        // create trie symbol table
        trie = new BoggleTrie();
        // populate trie
        for (String word : dictionary) {
            int len = word.length();
            // calculate word score base on its length
            int score;
            if      (len <= 2) score = 0;
            else if (len <= 4) score = 1;
            else if (len == 5) score = 2;
            else if (len == 6) score = 3;
            else if (len == 7) score = 5;
            else score = 11;
            // put word and its score in trie
            trie.put(word, score);
        }
    }

    /**
     * Returns the set of all valid words in the given Boggle board, as an Iterable.
     *
     * @param board the given Boggle board
     * @return the set of all valid words as an Iterable.
     */
    public Iterable<String> getAllValidWords(BoggleBoard board) {
        SET<String> words = new SET<>();
        // store dimensions of m x n board
        final int m = board.rows();
        final int n = board.cols();
        if (m == 0 || n == 0) return words;
        // reset visited positions from previous search
        visited = new boolean[m * n];
        // container for all letters in board
        final char[] letters = new char[m * n];
        // container for neighbors of each position
        final List<Iterable<Integer>> adj = new ArrayList<>(m * n);
        
        // precalculate letter in each tile along with neighbors
        for (int i = 0; i < m; i++) {
            // calculate horizontal boundary of neighbors
            final int nbRowStart = Math.max(i - 1, 0);
            final int nbRowEnd   = Math.min(i + 1, m - 1);
            for (int j = 0; j < n; j++) {
                // calculate vertical boundary of neighbors
                final int nbColStart = Math.max(j - 1, 0);
                final int nbColEnd   = Math.min(j + 1, n - 1);
                // add neighbors of current tile
                Bag<Integer> nbs = new Bag<>();
                for (int row = nbRowStart; row <= nbRowEnd; row++) {
                    for (int col = nbColStart; col <= nbColEnd; col++) {
                        nbs.add(row * n + col);
                    }
                }
                // store neighbors
                adj.add(nbs);
                // store letter in current tile
                letters[(i * n) + j] = board.getLetter(i, j);
            }
        }
        // declare string builder for prefix manipulation
        final StringBuilder sb = new StringBuilder(32);
        // run dfs on all tiles
        for (int idx = 0; idx < m * n; idx++) {
            dfs(adj, words, letters, idx, sb, trie);
        }
        // return collected words
        return words;
    }

    private void dfs(List<Iterable<Integer>> adj, SET<String> words, char[] letters, int idx, StringBuilder sb, BoggleTrie prev) {
        // retrieve letter in current position
        char letter = letters[idx];
        // get subtrie rooted at letter
        BoggleTrie subtrie = prev.step(letter);
        if (subtrie == null) return;
        // update prefix (performing another step if necessary)
        if (letter == 'Q') {
            subtrie = subtrie.step('U');
            if (subtrie == null) return;
            sb.append("QU");
        } else {
            sb.append(letter);
        };
        // if prefix consists of a word, record it
        if (subtrie.isEndofWord()) words.add(sb.toString());
        // mark tile as visited and continue search in unvisited neighbots
        visited[idx] = true;
        for (int next : adj.get(idx)) {
            if (!visited[next]) {
                dfs(adj, words, letters, next, sb, subtrie);
            }
        }
        // backtrack after recursive call returns
        visited[idx] = false;
        sb.deleteCharAt(sb.length() - 1);
        if (sb.length() > 0 && sb.charAt(sb.length() - 1) == 'Q')
            sb.deleteCharAt(sb.length() - 1);
    }

    /**
     * Returns the score of the given word if it is in the dictionary, zero otherwise
     *
     * @param word the given word
     * @return the score of the given word if it is in the dictionary, zero otherwise
     */
    public int scoreOf(String word) {
        // look for score in original, complete trie
        return trie.get(word);
    }

    public static void main(String[] args) {
        In in = new In(args[0]);
        String[] dictionary = in.readAllStrings();
        BoggleSolver solver = new BoggleSolver(dictionary);
        BoggleBoard board = new BoggleBoard(args[1]);
        int score = 0;
        for (String word : solver.getAllValidWords(board)) {
            StdOut.println(word);
            score += solver.scoreOf(word);
        }
        StdOut.println("Score = " + score);
    }
}