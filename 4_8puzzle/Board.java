import java.util.ArrayList;
import edu.princeton.cs.algs4.StdRandom;

public class Board {

    private final int n;    // board dimensions
    private int holeRow;    // row index of the blank square
    private int holeCol;    // column index of the blank square
    private int[][] tiles;
    
    // create a board from an n-by-n array of tiles,
    // where tiles[row][col] = tile at (row, col)
    public Board(int[][] tiles) {
        n = tiles.length;
        assert n >= 2 && n <= 128;
        // make defensive copy of passed in tiles
        this.tiles = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                this.tiles[i][j] = tiles[i][j];
                // track hole position
                if (this.tiles[i][j] == 0) {
                    holeRow = i;
                    holeCol = j;
                }
            }
        }
    }
                                           
    // string representation of this board`
    public String toString() {
        StringBuilder sb = new StringBuilder();
        // write first line with board size
        sb.append(n + "\n");
        // write board content
        for (int[] row : tiles) {
            for (int entry : row) {
                // insert board entry left padded with spaces for a total
                // length of three characters
                sb.append(" " + entry);
            }
            sb.append(" \n");
        }
        // remove last new space character added
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    // board dimension n
    public int dimension() {
        return n;
    }

    // number of tiles out of place
    public int hamming() {
        int count = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                final int entry = tiles[i][j];
                // we can tell if tiles greater than zero are in the correct
                // position if their value is greater than the index they
                // would have if the board was linearized by one 
                final int linearPosition = i * n + j;
                if (entry != 0 && (entry != linearPosition + 1)) count++;
            }
        }
        return count;
    }

    // sum of Manhattan distances between tiles and goal
    public int manhattan() {
        int sum = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                final int entry = tiles[i][j];
                if (entry == 0) continue;
                // a given entry should be at position entry - 1. Find the
                // (i, j) indexes associated with this target position
                final int targetPos = entry - 1;
                final int targetI = targetPos / n;
                final int targetJ = targetPos - (targetI * n);
                final int manhattan = Math.abs(targetI - i) + Math.abs(targetJ - j);
                sum += manhattan;
            }
        }
        return sum;
    }

    // is this board the goal board?
    public boolean isGoal() {
        return hamming() == 0;
    }

    // does this board equal y?
    public boolean equals(Object y) {
        if (y == this) return true;
        if (!(y instanceof Board)) return false;
        final Board that = (Board) y;
        // check if boards have the samy size
        if (n == that.n) {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    if (tiles[i][j] != that.tiles[i][j]) return false;
                }
            }
            return true;
        }
        return false;
    }

    // exchange pair of files
    private int[][] exchange(int i1, int j1, int i2, int j2) {
        final int[][] exchanged = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                exchanged[i][j] = tiles[i][j];
            }
        }
        
        final int tmp = exchanged[i1][j1];
        exchanged[i1][j1] = exchanged[i2][j2];
        exchanged[i2][j2] = tmp;

        return exchanged;
    }

    // all neighboring boards
    public Iterable<Board> neighbors() {
        ArrayList<Board> neighbors = new ArrayList<>();
        if (holeRow > 0) {
            final int[][] neighbor = exchange(holeRow, holeCol, holeRow - 1, holeCol);
            neighbors.add(new Board(neighbor));
        }
        if (holeRow < n - 1) {
            final int[][] neighbor = exchange(holeRow, holeCol, holeRow + 1, holeCol);
            neighbors.add(new Board(neighbor));
        }
        if (holeCol > 0) {
            final int[][] neighbor = exchange(holeRow, holeCol, holeRow, holeCol - 1);
            neighbors.add(new Board(neighbor));
        }
        if (holeCol < n - 1) {
            final int[][] neighbor = exchange(holeRow, holeCol, holeRow, holeCol + 1);
            neighbors.add(new Board(neighbor));
        }
        return neighbors;
    }

    // a board that is obtained by exchanging any pair of tiles
    public Board twin() {
        // arbitrarily swap first two tiles that are not equal to zero
        // (faster than generating random indexes) remembering n >= 2
        int i1, i2, j1, j2;
        if (tiles[0][0] != 0) {
            i1 = 0;
            j1 = 0;
            if (tiles[0][1] != 0) {
                i2 = 0;
                j2 = 1;
            } else {
                i2 = 2 / n;
                j2 = 2 % n;
            }
        } else {
            i1 = 0;
            j1 = 1;
            i2 = 2 / n;
            j2 = 2 % n;
        }
        final int[][] twin = exchange(i1, j1, i2, j2);
        return new Board(twin);
    }

    // unit testing (not graded)
    public static void main(String[] args) {
        final int[][] tiles1 = {{8, 1, 3},
                               {4, 0, 2},
                               {7, 6, 5}};
        final Board b1 = new Board(tiles1);
        System.out.println(b1);
        assert b1.hamming() == 5;
        assert b1.manhattan() == 10;
        Board b1Copy = new Board(tiles1);
        assert !b1.equals(tiles1);
        assert b1.equals(b1Copy);
        // initialize board with different tiles
        final int[][] tiles2 = {{1, 0, 3},
                                {4, 2, 5},
                                {7, 8, 6}};
        final Board b2 = new Board(tiles2);
        assert b2.hamming() == 3;
        assert b2.manhattan() == 3;
        // write tiles for bottom neighbor
        final int[][] bottomTiles = {{1, 2, 3},
                                     {4, 0, 5},
                                     {7, 8, 6}};
        // write tiles for left neighbor
        final int[][] leftTiles = {{0, 1, 3},
                                   {4, 2, 5},
                                   {7, 8, 6}};
        // write tiles for right neighbor
        final int[][] rightTiles = {{1, 3, 0},
                                    {4, 2, 5},
                                    {7, 8, 6}};
        final Iterable<Board> nbs = b2.neighbors();
        final Board[] nbsCheck = {new Board(bottomTiles),
                                  new Board(leftTiles),
                                  new Board(rightTiles)};
        int curr = 0;
        for (Board nb : nbs) {
            assert nb.equals(nbsCheck[curr++]);
        }
        assert curr == 3;
        // check whether original board remained unchanged
        final Board b2Copy = new Board(tiles2);
        assert b2.equals(b2Copy);
        
        System.out.println("** PASSED ALL TESTS! **");
    }

}