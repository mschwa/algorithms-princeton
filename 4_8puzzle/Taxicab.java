/******************************************************************************
 *  Compilation:  javac Taxicab.java
 *  Execution:    java Taxicab n
 *  Dependencies: StdOut.java MinPQ.java
 * 
 *  Find all nontrivial integer solutions to a^3 + b^3 = c^3 + d^3 where
 *  a, b, c, and d are between 0 and n. By nontrivial, we mean
 *  a <= b, c <= d, and a < c.
 *
 *  % java Taxicab 11
 *
 *  % java Taxicab 12
 *  1729 = 1^3 + 12^3 = 9^3 + 10^3
 *
 *  % java Taxicab 1000
 *  1729 = 1^3 + 12^3 = 9^3 + 10^3
 *  4104 = 2^3 + 16^3 = 9^3 + 15^3
 *  13832 = 2^3 + 24^3 = 18^3 + 20^3
 *  20683 = 10^3 + 27^3 = 19^3 + 24^3
 *  32832 = 4^3 + 32^3 = 18^3 + 30^3
 *  ...
 *  87539319 = 167^3 + 436^3 = 228^3 + 423^3 = 255^3 + 414^3
 *  ...
 *  1477354411 = 802^3 + 987^3 = 883^3 + 924^3
 * 
 *  At all moments the PQ maintained in the algorithm has at most n pairs,
 *  so the space complexity is O(n).
 * 
 *  The time complexity of the algorithm can be approximated as follows: at each
 *  step, we perform one delMin and one insert operation on the MinPQ. Both are
 *  O(logn) operation, so the two combined are O(logn) as well. Because we inspect
 *  n^2/2 elements (one half of the matrix), the are ~n^2 such steps, yielding
 *  a O(n^2 log n) complexity.
 *
 ******************************************************************************/

import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.MinPQ;

public class Taxicab implements Comparable<Taxicab> {
    private final int i;
    private final int j;
    private final long sum;   // i^3 + j^3, cached to avoid recomputation

    // create a new tuple (i, j, i^3 + j^3)
    public Taxicab(int i, int j) {
        this.sum = (long) i*i*i + (long) j*j*j;
        this.i = i;
        this.j = j;
    }

    // compare by i^3 + j^3, breaking ties by i
    public int compareTo(Taxicab that) {
        if      (this.sum < that.sum) return -1;
        else if (this.sum > that.sum) return +1;
        else if (this.i < that.i)     return -1;
        else if (this.i > that.i)     return +1;
        else                          return  0;
    }

    public String toString() {
        return i + "^3 + " + j + "^3";
    }


    public static void main(String[] args) {

        // find a^3 + b^3 = c^3 + d^3, where a, b, c, d <= n
        int n = Integer.parseInt(args[0]);

        // initialize priority queue with diagonal elements of the following
        // illustration: 
        //     1     2    3     4 ...
        // ---------------------------
        // 1 | 2     9   28    65
        // 2 | 9    16   35    72
        // 3 | 28   35   54    91
        // 4 | 65   72   91   128
        // 
        // This guarantees that the initial PQ will have an element an element
        // from each line. By only incrementing j later in the algorithm, we
        // can use this property to scan all sums of cubes without going over
        // the repeated half of the matrix and also make sure all elements
        MinPQ<Taxicab> pq = new MinPQ<Taxicab>();
        for (int i = 1; i <= n; i++) {
            pq.insert(new Taxicab(i, i));
        }

        int match = 1;                      // number of pairs with same sum of cubes as current pair
        Taxicab prev = new Taxicab(0, 0);   // initialize to sentinel
        // The gist of the algorithm goes as follows: we maintain a MinPQ
        // with n (i, j) pairs prioritized by their cube sum. At each
        // step, we pop the pair with the lowest cube sum not seen yet.
        // Given the cube sum prioritazation, if there's any pair whose
        // cube sum equals the last one inspected, it must be the one
        // just popped. This procedure continues until the entire left half
        // of the matrix (above) has been inspected.
        while (!pq.isEmpty()) {
            Taxicab curr = pq.delMin();

            // current sum is same as previous sum
            if (prev.sum == curr.sum) {
                match++;
                if (match == 2) StdOut.print(prev.sum + " = " + prev);
                StdOut.print(" = " + curr);
            }
            else {
                if (match > 1) StdOut.println();
                match = 1;
            }
            prev = curr;

            if (curr.j < n) pq.insert(new Taxicab(curr.i, curr.j + 1));
        }
        if (match > 1) StdOut.println();
    }

}


// Copyright © 2000–2017, Robert Sedgewick and Kevin Wayne.
// Last updated: Fri Oct 20 12:50:46 EDT 2017.
