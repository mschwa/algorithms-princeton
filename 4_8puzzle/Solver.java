import java.util.Comparator;
import java.util.ArrayList;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.MinPQ;

public class Solver {

    private Board[] solution;
    
    // find a solution to the initial board (using the A* algorithm)
    public Solver(Board initial) {
        if (initial == null) throw new IllegalArgumentException();
        // initialize class members
        solution = null;
        // A* seek solution
        aStar(initial, initial.twin());
    }

    private class SearchNode {
        private Board board;
        private int moves;
        private int priority;
        private SearchNode prevNode;
    }

    private class NodeComparator implements Comparator<SearchNode> {
        public int compare(SearchNode n1, SearchNode n2) {
            if (n1.priority < n2.priority) return -1;
            if (n1.priority > n2.priority) return +1;
            return 0;
        }
    }

    private void advanceSearch(MinPQ<SearchNode> pq) {
        final SearchNode node = pq.delMin();
        // get neighbors of board in node just popped
        final Iterable<Board> neighbors = node.board.neighbors();
        for (Board nb : neighbors) {
            // critical optmization: don’t enqueue a neighbor if its board
            // is the same as the board of the previous search node in the
            // game tree
            if ((node.prevNode != null) && nb.equals(node.prevNode.board)) continue;
            // calculate neightbor manhattan distanceZ
            // enqueue successpr
            final SearchNode successor = new SearchNode();
            successor.board = nb;
            successor.moves = node.moves + 1;
            successor.priority = successor.moves + nb.manhattan();
            successor.prevNode = node;
            pq.insert(successor);
        }
    }

    // perform aStar search for 8-puzzle solution. The total estimated
    // cost is the sum of the backward cost (numer of moves so far) plus
    // the estimated cost given by the manhattan distance heuristic
    private void aStar(Board initial, Board twin) {
        final NodeComparator comparator = new NodeComparator();
        final MinPQ<SearchNode> pqOriginal = new MinPQ<>(comparator);
        final MinPQ<SearchNode> pqTwin = new MinPQ<>(comparator);
        // build initial search node
        final SearchNode startOriginal = new SearchNode();
        startOriginal.board = initial;
        startOriginal.moves = 0;
        startOriginal.priority = initial.manhattan();
        startOriginal.prevNode = null;
        pqOriginal.insert(startOriginal);
        // build initial search node
        final SearchNode startTwin = new SearchNode();
        startTwin.board = twin;
        startTwin.moves = 0;
        startTwin.priority = twin.manhattan();
        startTwin.prevNode = null;
        pqTwin.insert(startTwin);
        // start A* traversal
        while (!pqOriginal.min().board.isGoal() && !pqTwin.min().board.isGoal()) {
            advanceSearch(pqOriginal);
            advanceSearch(pqTwin);
        }
        if (pqOriginal.min().board.isGoal()) {
            SearchNode node = pqOriginal.delMin();
            // initialize the array containing the board progression
            // towards the solution
            solution = new Board[node.moves + 1];
            // populate the array from end to start by iterating over
            // search nodes
            int i = node.moves;
            while (node != null) {
                solution[i--] = node.board;
                node = node.prevNode;
            }
            return;
        }
    }

    // is the initial board solvable? (see below)
    public boolean isSolvable() {
        return solution != null;
    }

    // min number of moves to solve initial board
    public int moves() {
        return isSolvable() ? solution.length - 1 : -1;
    }

    // sequence of boards in a shortest solution
    public Iterable<Board> solution() {
        if (!isSolvable()) return null;
        final ArrayList<Board> iterableSolution = new ArrayList<>();
        for (int i = 0; i < solution.length; i++) {
            iterableSolution.add(solution[i]);
        }
        return iterableSolution;
    }

    // test client (see below) 
    public static void main(String[] args)  {
        // create initial board from file
        In in = new In(args[0]);
        int n = in.readInt();
        int[][] tiles = new int[n][n];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                tiles[i][j] = in.readInt();
        Board initial = new Board(tiles);

        // solve the puzzle
        Solver solver = new Solver(initial);

        // print solution to standard output
        if (!solver.isSolvable())
            StdOut.println("No solution possible");
        else {
            StdOut.println("Minimum number of moves = " + solver.moves());
            for (Board board : solver.solution())
                StdOut.println(board);
        }
    }

}