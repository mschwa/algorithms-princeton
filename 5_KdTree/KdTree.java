import java.util.ArrayList;

import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;

public class KdTree {

    private Node root;

    private class Node {
        private Point2D point;
        private boolean vertical;
        private Node left, right;
        private int size;

        public Node(Point2D point, boolean vertical, int size) {
            this.point = point;
            this.vertical = vertical;
            this.size = size;
        }
    }

    // construct an empty set of points 
    public KdTree() { }

    // is the set empty?
    public boolean isEmpty() {
        return size(root) == 0;
    }

    // number of points in the set
    public int size() {
        return size(root);
    }

    private int size(Node n) {
        if (n == null) return 0;
        else return n.size;
    }

    // add the point to the set (if it is not already in the set)
    public void insert(Point2D p) {
        if (p == null) throw new IllegalArgumentException();
        if (contains(p)) return;
        root = insert(root, p, false);
    }

    private Node insert(Node n, Point2D p, boolean parentVertical) {
        if (n == null) return new Node(p, !parentVertical, 1);
        // for vertical node, check whether point's x-coordinate is less than
        // that of current node. For horizontal node, check wheter point's
        // y-coordinate is greater than that of current node
        final boolean smaller = n.vertical
            ? n.point.x() > p.x()
            : n.point.y() > p.y();
        if (smaller) n.left = insert(n.left, p, n.vertical);
        else n.right = insert(n.right, p, n.vertical);
        // update size of tree rooted in node on the way up
        n.size = 1 + size(n.left) + size(n.right);
        return n;
    }

    // does the set contain point p?
    public boolean contains(Point2D p) {
        if (p == null) throw new IllegalArgumentException();
        return contains(root, p);
    }

    // use recursion to reach position where node should be. If recursion finds
    // node with given point there is a hit. Else, if recursion ends at null
    // node, there is a miss.
    private boolean contains(Node n, Point2D p) {
        if (n == null) return false;
        if (n.point.compareTo(p) == 0) return true;
        // for vertical node, check whether point's x-coordinate is less than
        // that of current node. For horizontal node, check wheter point's
        // y-coordinate is greater than that of current node
        final boolean smaller = n.vertical
            ? n.point.x() > p.x()
            : n.point.y() > p.y();
        if (smaller) return contains(n.left, p);
        else return contains(n.right, p);
    }

    // draw all points to standard draw 
    public void draw() {
        draw(root, 0, 1, 0, 1);
    }

    private void draw(Node n, double xLower, double xUpper, double yLower, double yUpper) {
        if (n == null) return;
        StdDraw.setPenRadius(0.005);
        if (n.vertical) {
            // draw vertical line
            StdDraw.setPenColor(StdDraw.RED);
            StdDraw.line(n.point.x(), yLower, n.point.x(), yUpper);
            // update x bounds that horizontal children's lines can occupy
            draw(n.left, xLower, n.point.x(), yLower, yUpper);
            draw(n.right, n.point.x(), xUpper, yLower, yUpper);
        } else {
            // draw horizontal line
            StdDraw.setPenColor(StdDraw.BLUE);
            StdDraw.line(xLower, n.point.y(), xUpper, n.point.y());
            // update y bound that vertical children's line can occupy
            draw(n.left, xLower, xUpper, yLower, n.point.y());
            draw(n.right, xLower, xUpper, n.point.y(), yUpper);
        }
        // draw points on the way up so that they are drawn on top of lines
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.setPenRadius(0.02);
        n.point.draw();
    }

    // all points that are inside the rectangle (or on the boundary)
    public Iterable<Point2D> range(RectHV rect) {
        if (rect == null) throw new IllegalArgumentException();
        final ArrayList<Point2D> range = new ArrayList<>();
        range(root, rect, range);
        return range;
    }

    private void range(Node n, RectHV rect, ArrayList<Point2D> range) {
        if (n == null) return;
        // if node point is contained in rectangle, add point to set and
        // inspect points in its left and right subtrees
        if (rect.contains(n.point)) {
            range.add(new Point2D(n.point.x(), n.point.y()));
            range(n.left, rect, range);
            range(n.right, rect, range);
        // if node point is not contained in rectangle, only inspect subtree
        // that could contain it
        } else {
            // for a given partitioning dimension (x or y), is the point's
            // coordinate greater than the rectangle's maximum coordinate for
            // that dimension?
            final boolean pointRight = n.vertical
                ? n.point.x() > rect.xmax()
                : n.point.y() > rect.ymax();
            // for a given partitioning dimension (x or y), is the point's
            // coordinate less than the rectangle's minimum coordinate for
            // that dimension?
            final boolean pointLeft = n.vertical 
                ? n.point.x() < rect.xmin()
                : n.point.y() < rect.ymin();
            // search subtrees according to the tested scenarios
            if (pointRight) range(n.left, rect, range);
            else if (pointLeft) range(n.right, rect, range);
            // if point coordinate is within the rectangle bounds for that
            // dimension, points in the reage could be on either subtrees
            else {
                range(n.left, rect, range);
                range(n.right, rect, range);
            }
        }
    }

    // private class to hold the result of a nearest search
    private class Champion {
        private Point2D point;
        private double dist;
    }

    // a nearest neighbor in the set to point p; null if the set is empty
    public Point2D nearest(Point2D p) {
        if (p == null) throw new IllegalArgumentException();
        if (isEmpty()) return null;
        // initialize champion
        final Champion champion = new Champion();
        champion.dist = Double.POSITIVE_INFINITY;
        // INSTEAD OF DELTA, CALCULATE DISTANCE FROM POINT TO RECTANGLE!
        // find overall champion
        nearest(root, p, champion, 0, 0, 1, 1);
        return new Point2D(champion.point.x(), champion.point.y());
    }

    private void nearest(Node n, Point2D p, Champion champion,
                         double xmin, double ymin, double xmax, double ymax) {
        if (n == null) return;
        // calculate distance between current node point and query point
        final double dist = n.point.distanceTo(p);
        if (dist < champion.dist) {
            // update champion
            champion.point = n.point;
            champion.dist = dist;
        }
        // determine whether left child subtree initially is the one to be
        // initially searched
        final boolean left = n.vertical
            ? n.point.x() > p.x()
            : n.point.y() > p.y();
        // declare rectangle enclosing subtree *not* visited initially
        RectHV deferredRect;
        // visit rectangle enclosing the query point and define boundaries
        // of deferred rectangle
        if (n.vertical) {
            if (left) {
                deferredRect = new RectHV(n.point.x(), ymin, xmax, ymax);
                nearest(n.left, p, champion, xmin, ymin, n.point.x(), ymax);
            }
            else {
                deferredRect = new RectHV(xmin, ymin, n.point.x(), ymax);
                nearest(n.right, p, champion, n.point.x(), ymin, xmax, ymax);
            }
        } else {
            if (left) {
                deferredRect = new RectHV(xmin, n.point.y(), xmax, ymax);
                nearest(n.left, p, champion, xmin, ymin, xmax, n.point.y());
            }
            else {
                deferredRect = new RectHV(xmin, ymin, xmax, n.point.y());
                nearest(n.right, p, champion, xmin, n.point.y(), xmax, ymax);
            }
        }
        // on the way back from traversal of first child, calculate distance
        // form query point to the deferred rectangle. If this distance is
        // smaller than the champion distance, it is possible that a new
        // champion is in the deferred rectangle
        if (deferredRect.distanceTo(p) < champion.dist) {
            if (n.vertical) {
                if (left) nearest(n.right, p, champion, n.point.x(), ymin, xmax, ymax);
                else nearest(n.left, p, champion, xmin, ymin, n.point.x(), ymax);
            } else {
                if (left) nearest(n.right, p, champion, xmin, n.point.y(), xmax, ymax);
                else nearest(n.left, p, champion, xmin, ymin, xmax, n.point.y());
            }
        }
    }

    // unit testing of the methods (optional)
    public static void main(String[] args) {
        // create points forming a rectangle
        final Point2D p1 = new Point2D(0.7, 0.2);
        final Point2D p2 = new Point2D(0.5, 0.4);
        final Point2D p3 = new Point2D(0.2, 0.3);
        final Point2D p4 = new Point2D(0.4, 0.7);
        final Point2D p5 = new Point2D(0.9, 0.6);
        // create set and populate it with rectangle vertices
        final KdTree set = new KdTree();
        final Point2D[] pts = { p1, p2, p3, p4, p5 };
        assert set.isEmpty();
        int count = 0;
        for (Point2D p: pts) {
            set.insert(p);
            assert set.size() == ++count;
            for (int i = 0; i < count; i++) {
                assert set.contains(pts[i]);
            }
        }
        set.draw();

        // perform basic range tests
        final RectHV rect1 = new RectHV(0.45, 0.15, 0.75, 0.45);
        // rect1.draw();
        Iterable<Point2D> range = set.range(rect1);
        final Point2D[] expected1 = { p1, p2 };
        count = 0;
        for (Point2D p: range) { assert p.equals(expected1[count++]); }
        assert count == 2;
        //-- 
        final RectHV rect2 = new RectHV(0.35, 0.15, 0.95, 0.75);
        // rect2.draw();
        range = set.range(rect2);
        final Point2D[] expected2 = { p1, p2, p4, p5 };
        count = 0;
        for (Point2D p: range) { assert p.equals(expected2[count++]); }
        assert count == 4;
        // --
        final RectHV rect3 = new RectHV(0.45, 0.15, 0.95, 0.75);
        // rect3.draw();
        range = set.range(rect3);
        final Point2D[] expected3 = { p1, p2, p5 };
        count = 0;
        for (Point2D p: range) { assert p.equals(expected3[count++]); }
        assert count == 3;

        // perform basic nearest tests
        assert set.nearest(new Point2D(0.71, 0.21)).equals(p1);
        assert set.nearest(new Point2D(0, 0)).equals(p3);
        assert set.nearest(new Point2D(1, 1)).equals(p5);
        assert set.nearest(new Point2D(0.5, 0.5)).equals(p2);

        final Point2D r1 = new Point2D(0.7, 0.2);
        final Point2D r2 = new Point2D(0.5, 0.4);
        final Point2D r3 = new Point2D(0.2, 0.3);
        final Point2D r4 = new Point2D(0.4, 0.7);
        final Point2D r5 = new Point2D(0.9, 0.6);
        final Point2D[] randPts = { r1, r2, r3, r4, r5};
        final KdTree randSet = new KdTree();
        count = 0;
        for (Point2D pt : randPts) {
            randSet.insert(randPts[count++]);
        }
        StdDraw.clear();
        randSet.draw();
        StdDraw.setPenColor(StdDraw.GREEN);
        final Point2D query = new Point2D(0.41, 0.1);
        System.out.println("\n====================================================\n");
        randSet.nearest(query);
        query.draw();

        
        
        System.out.println("** TESTS COMPLETED SUCCESSFULLY **");
    }
}