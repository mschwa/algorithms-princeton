import java.util.ArrayList;

import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.SET;

public class PointSET {

    private SET<Point2D> set;    // balance search tree for inner representation of set

    // construct an empty set of points 
    public PointSET() {
        set = new SET<>();
    }

    // is the set empty?
    public boolean isEmpty() {
        return set.isEmpty();
    }

    // number of points in the set
    public int size() {
        return set.size();
    }

    // add the point to the set (if it is not already in the set)
    public void insert(Point2D p) {
        if (p == null) throw new IllegalArgumentException();
        set.add(p);
    }

    // does the set contain point p?
    public boolean contains(Point2D p) {
        if (p == null) throw new IllegalArgumentException();
        return set.contains(p);
    }

    // draw all points to standard draw 
    public void draw() {
        for (Point2D p : set) {
            p.draw();
        }
    }

    // all points that are inside the rectangle (or on the boundary)
    public Iterable<Point2D> range(RectHV rect) {
        if (rect == null) throw new IllegalArgumentException();
        final ArrayList<Point2D> points = new ArrayList<>();
        for (Point2D p : set) {
            if (rect.contains(p)) points.add(p);
        }
        return points;
    }

    // a nearest neighbor in the set to point p; null if the set is empty
    public Point2D nearest(Point2D p) {
        if (p == null) throw new IllegalArgumentException();
        if (isEmpty()) return null;
        double minDistance = Double.POSITIVE_INFINITY;
        Point2D nearest = null;
        for (Point2D pt : set) {
            final double dist = p.distanceTo(pt);
            if (dist < minDistance) {
                minDistance = dist;
                nearest = new Point2D(pt.x(), pt.y());
            }
        }
        return nearest;
    }

    // unit testing of the methods (optional)
    public static void main(String[] args) {
        // create points forming a rectangle
        final Point2D p1 = new Point2D(0.25, 0.25);
        final Point2D p2 = new Point2D(0.75, 0.25);
        final Point2D p3 = new Point2D(0.25, 0.75);
        final Point2D p4 = new Point2D(0.75, 0.75);
        // create set and populate it with rectangle vertices
        final PointSET set = new PointSET();
        assert set.isEmpty();
        assert !set.contains(p1);
        assert !set.contains(p2);
        assert !set.contains(p3);
        assert !set.contains(p4);
        assert set.nearest(p1) == null;
        set.insert(p1);
        assert !set.isEmpty();
        assert set.size() == 1;
        assert set.contains(p1);
        assert !set.contains(p2);
        assert !set.contains(p3);
        assert !set.contains(p4);
        set.insert(p2);
        assert !set.isEmpty();
        assert set.size() == 2;
        assert set.contains(p1);
        assert set.contains(p2);
        assert !set.contains(p3);
        assert !set.contains(p4);
        set.insert(p3);
        assert !set.isEmpty();
        assert set.size() == 3;
        assert set.contains(p1);
        assert set.contains(p2);
        assert set.contains(p3);
        assert !set.contains(p4);
        set.insert(p4);
        assert !set.isEmpty();
        assert set.size() == 4;
        assert set.contains(p1);
        assert set.contains(p2);
        assert set.contains(p3);
        assert set.contains(p4);
        // create rectangles and perform range tests
        final RectHV rect1 = new RectHV(0.1, 0.1, 0.9, 0.4);
        Iterable<Point2D> range = set.range(rect1);
        final Point2D[] expected1 = { p1, p2 };
        int count = 0;
        for (Point2D p: range) { assert p.equals(expected1[count++]); }
        assert count == 2;
        // --
        final RectHV rect2 = new RectHV(0.1, 0.1, 0.4, 0.9);
        range = set.range(rect2);
        final Point2D[] expected2 = { p1, p3 };
        count = 0;
        for (Point2D p: range) { assert p.equals(expected2[count++]); }
        assert count == 2;
        // --
        final RectHV rect3 = new RectHV(0.1, 0.6, 0.9, 0.9);
        range = set.range(rect3);
        final Point2D[] expected3 = { p3, p4 };
        count = 0;
        for (Point2D p: range) { assert p.equals(expected3[count++]); }
        assert count == 2;
        // --
        final RectHV rect4 = new RectHV(0.6, 0.1, 0.9, 0.9);
        range = set.range(rect4);
        final Point2D[] expected4 = { p2, p4 };
        count = 0;
        for (Point2D p: range) { assert p.equals(expected4[count++]); }
        assert count == 2;
        // --
        final RectHV rect5 = new RectHV(0.1, 0.1, 0.9, 0.9);
        range = set.range(rect5);
        final Point2D[] expected5 = { p1, p2, p3, p4 };
        count = 0;
        for (Point2D p: range) { assert p.equals(expected5[count++]); }
        assert count == 4;
        // --
        final RectHV rect6 = new RectHV(0.4, 0.4, 0.6, 0.6);
        range = set.range(rect6);
        count = 0;
        for (Point2D p: range) { count++; }
        assert count == 0;
        // --
        final RectHV rect7 = new RectHV(0.2, 0.2, 0.3, 0.3);
        range = set.range(rect7);
        final Point2D[] expected6 = { p1 };
        count = 0;
        for (Point2D p: range) { assert p.equals(expected6[count++]); }
        assert count == 1;
        // --
        final RectHV rect8 = new RectHV(0.7, 0.2, 0.9, 0.4);
        range = set.range(rect8);
        final Point2D[] expected7 = { p2 };
        count = 0;
        for (Point2D p: range) { assert p.equals(expected7[count++]); }
        assert count == 1;
        // --
        final RectHV rect9 = new RectHV(0.2, 0.7, 0.3, 0.9);
        range = set.range(rect9);
        final Point2D[] expected8 = { p3 };
        count = 0;
        for (Point2D p: range) { assert p.equals(expected8[count++]); }
        assert count == 1;
        // --
        final RectHV rect10 = new RectHV(0.7, 0.7, 0.9, 0.9);
        range = set.range(rect10);
        final Point2D[] expected9 = { p4 };
        count = 0;
        for (Point2D p: range) { assert p.equals(expected9[count++]); }
        assert count == 1;
        // insert point and repeat range tests
        final Point2D p5 = new Point2D(0.8, 0.8);
        set.insert(p5);
        // --
        range = set.range(rect1);
        final Point2D[] expected10 = { p1, p2 };
        count = 0;
        for (Point2D p: range) { assert p.equals(expected10[count++]); }
        assert count == 2;
        // --
        range = set.range(rect2);
        final Point2D[] expected11 = { p1, p3 };
        count = 0;
        for (Point2D p: range) { assert p.equals(expected11[count++]); }
        assert count == 2;
        // --
        range = set.range(rect3);
        final Point2D[] expected12 = { p3, p4, p5 };
        count = 0;
        for (Point2D p: range) { assert p.equals(expected12[count++]); }
        assert count == 3;
        // --
        range = set.range(rect4);
        final Point2D[] expected13 = { p2, p4, p5 };
        count = 0;
        for (Point2D p: range) { assert p.equals(expected13[count++]); }
        assert count == 3;
        // --
        range = set.range(rect5);
        final Point2D[] expected14 = { p1, p2, p3, p4, p5 };
        count = 0;
        for (Point2D p: range) { assert p.equals(expected14[count++]); }
        assert count == 5;
        // --
        range = set.range(rect6);
        count = 0;
        for (Point2D p: range) { count++; }
        assert count == 0;
        // --
        range = set.range(rect7);
        final Point2D[] expected15 = { p1 };
        count = 0;
        for (Point2D p: range) { assert p.equals(expected15[count++]); }
        assert count == 1;
        // --
        range = set.range(rect8);
        final Point2D[] expected16 = { p2 };
        count = 0;
        for (Point2D p: range) { assert p.equals(expected16[count++]); }
        assert count == 1;
        // --
        range = set.range(rect9);
        final Point2D[] expected17 = { p3 };
        count = 0;
        for (Point2D p: range) { assert p.equals(expected17[count++]); }
        assert count == 1;
        // --
        range = set.range(rect10);
        final Point2D[] expected18 = { p4, p5 };
        count = 0;
        for (Point2D p: range) { assert p.equals(expected18[count++]); }
        assert count == 2;
        // test for nearest neighbor
        assert set.nearest(new Point2D(0.25, 0.35)).equals(p1);
        assert set.nearest(new Point2D(0.5, 0.5)).equals(p1);
        assert set.nearest(new Point2D(0.25, 0.25)).equals(p1);
        assert set.nearest(new Point2D(0.9, 0.9)).equals(p5);

        StdDraw.setPenRadius(0.02);
        set.draw();
        
        System.out.println("** TESTS COMPLETED SUCCESSFULLY **");
    }
}