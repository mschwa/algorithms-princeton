import java.util.HashMap;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.FlowEdge;
import edu.princeton.cs.algs4.FlowNetwork;
import edu.princeton.cs.algs4.FordFulkerson;

public class BaseballElimination {
    private final int numberOfTeams;                    // number of teams in division
    private final String[] teams;                       // teams[i] = name of ith team in division
    private final HashMap<String, Integer> teamIndices; // teamIndices[team] = index associated with team
    private final int[] wins;                           // wins[i] = number of wins by ith team
    private final int[] losses;                         // losses[i] = number of losses by ith team
    private final int[] remaining;                      // remaining[i] = number of games remaining for ith team across all divisions
    private final int[][] games;                        // games[i][j] number of games team left for teams i, j (same division) to play
    private FlowNetwork G;                              // internal representation of flow network
    private int lastIdx;                                // memoization: index of last queried team
    private boolean wasLastEliminated;                  // memoization: was last queried team team concluded to be eliminated?
    private Queue<String> lastCut;                      // memoization: cut responsible for elimination of last queried team

    // create a baseball division from given filename in format specified below
    public BaseballElimination(String filename) {
        if (filename == null) throw new IllegalArgumentException();
        // read in flie
        In in = new In(filename);
        // extract number of teams from first line
        numberOfTeams = Integer.parseInt(in.readLine());
        // create internal collections based on read number of teams
        teams = new String[numberOfTeams];
        teamIndices = new HashMap<>(numberOfTeams);
        wins = new int[numberOfTeams];
        losses = new int[numberOfTeams];
        remaining = new int[numberOfTeams];
        games = new int[numberOfTeams][numberOfTeams];
        // parse table entries to populate collections
        int i = 0;
        while (in.hasNextLine()) {
            // remove leading and trailing whitespaces
            final String line = in.readLine().trim();
            // consider sequence of whiteline characters as single split point
            final String[] data = line.split(" +");
            // access data from split
            teams[i] = data[0];
            teamIndices.put(data[0], i);
            wins[i] = Integer.parseInt(data[1]);
            losses[i] = Integer.parseInt(data[2]);
            remaining[i] = Integer.parseInt(data[3]);
            for (int j = 0; j < numberOfTeams; j++) {
                games[i][j] = Integer.parseInt(data[4 + j]);
            }
            i += 1;
        }
        // initialize memoization members
        lastIdx = -1;
        lastCut = new Queue<>();
    }

    // number of teams
    public int numberOfTeams() {
        return numberOfTeams;
    }

    // all teams
    public Iterable<String> teams() {
        final Queue<String> copy = new Queue<>();
        for (int i = 0; i < numberOfTeams; i++) {
            copy.enqueue(teams[i]);
        }
        return copy;
    }

    private int validateTeam(String team) {
        if (team == null) throw new IllegalArgumentException();
        final Integer idx = teamIndices.get(team);
        if (idx == null) throw new IllegalArgumentException();
        return idx;
    }

    // number of wins for given team
    public int wins(String team) {
        final int idx = validateTeam(team);
        return wins[idx];
    }

    // number of losses for given team
    public int losses(String team) {
        final int idx = validateTeam(team);
        return losses[idx];
    }

    // number of remaining games for given team
    public int remaining(String team) {
        final int idx = validateTeam(team);
        return remaining[idx];
    }

    // number of remaining games between team1 and team2
    public int against(String team1, String team2) {
        final int idx1 = validateTeam(team1);
        final int idx2 = validateTeam(team2);
        return games[idx1][idx2];   
    }

    private boolean triviallyEliminated(int queriedTeamIndex) {
        // calculate maximum possible number of wins for team
        final int maxWins = wins[queriedTeamIndex] + remaining[queriedTeamIndex];
        // check if any team has already won more games than this maximum
        boolean eliminated = false;
        for (int i = 0; i < numberOfTeams; i++) {
            if (wins[i] > maxWins) {
                eliminated = true;
                lastCut.enqueue(teams[i]);
            }
        }
        return eliminated;
    }

    private void buildAndSolveNetwork(int queriedTeamIndex) {
        // calculate number of matches left between other teams: C(n,2) =
        // n!/(2 * (n-2)!) = n * (n-1)/2
        final int matchCount = (numberOfTeams - 1) * (numberOfTeams - 2) / 2;
        // calculate number of vertices in network representation
        final int V = 2 + matchCount + (numberOfTeams - 1);
        // initialize flow network
        G = new FlowNetwork(V);
        final int source = 0;
        final int target = V - 1;
        // keep reference to first vertex associated with a team (rather than
        // a match)
        final int firstTeamVertex = matchCount + 1;
        // declare counter tracking match vertex number (m) as well as cursors
        // for acessing teams playing that match (i and j)
        int m = 1;
        int i = 0;
        int j = 1;
        // calculate base capacity used to determine the final capacity of
        // edges leading to target
        final int baseCapacity = wins[queriedTeamIndex] + remaining[queriedTeamIndex];
        // build network from division table
        for (int row = 0; row < numberOfTeams; row++) {
            if (row == queriedTeamIndex) continue;
            // calculate value of vertex associated with team1
            final int vertexTeam1 = firstTeamVertex + i;
            // find unique matches team1 is playing
            for (int col = row + 1; col < numberOfTeams; col++) {
                if (col == queriedTeamIndex) continue;
                // calculate value of vertex associated with team2
                final int vertexTeam2 = firstTeamVertex + j;
                // add edge from source to match vertex
                final FlowEdge e = new FlowEdge(source, m, games[row][col]);
                G.addEdge(e);
                // add edges from match vertex to vertices of teams playing it
                final FlowEdge e1 = new FlowEdge(m, vertexTeam1, Integer.MAX_VALUE);
                final FlowEdge e2 = new FlowEdge(m, vertexTeam2, Integer.MAX_VALUE);
                G.addEdge(e1); G.addEdge(e2);
                // update number and avance team2 cursor
                m ++; j++;
            }
            // advance team1 cursor and reset team2 cursor
            i++;
            j = i + 1;
            // add edges from teams vertices to target vertex
            final FlowEdge e3 = new FlowEdge(vertexTeam1, target, baseCapacity - wins[row]);
            G.addEdge(e3);
        }
        // once network is built, solve maxflow problem
        final FordFulkerson FF = new FordFulkerson(G, 0, G.V() - 1);
        // declare cursor for accessing names of teams in network
        int c = 0;
        // get vertices in cut
        for (int t = 0; t < numberOfTeams - 1; t++) {
            // store name of team vertex in cut
            if (FF.inCut(firstTeamVertex + t)) lastCut.enqueue(teams[c]);
            // advance cursor, skipping queriedTeamIndex
            c += 1;
            if (c == queriedTeamIndex) c++;
        }
    }

    // // is given team eliminated?
    public boolean isEliminated(String team) {
        final int idx = validateTeam(team);
        // memoization
        if (idx != lastIdx) {
            // reset memoization variables
            lastCut = new Queue<>();
            wasLastEliminated = false;
            lastIdx = idx;
            // check for trivial elimination
            if (triviallyEliminated(idx)) {
                wasLastEliminated = true;
                return true;
            }
            // if no trivial elimination, check elimination with maxflow-mincut
            buildAndSolveNetwork(idx);
            // check if edges incident on source are full
            for (FlowEdge e: G.adj(0)) {
                if (e.flow() != e.capacity()) {
                    wasLastEliminated = true;
                    return true;
                }
            }
            return wasLastEliminated;

        } else return wasLastEliminated;
    }

    // subset R of teams that eliminates given team; null if not eliminated
    public Iterable<String> certificateOfElimination(String team) {
        final int idx = validateTeam(team);
        // memoization: only recompute elimination if necessary
        if (idx != lastIdx) isEliminated(team);
        // build iterable with teams in cut
        final Queue<String> copy = new Queue<>();
        for (String t: lastCut) copy.enqueue(t);
        if (copy.size() > 0) return copy;
        else return null;
    }

    public static void main(String[] args) {
        BaseballElimination division = new BaseballElimination(args[0]);
        for (String team : division.teams()) {
            if (division.isEliminated(team)) {
                StdOut.print(team + " is eliminated by the subset R = { ");
                for (String t : division.certificateOfElimination(team)) {
                    StdOut.print(t + " ");
                }
                StdOut.println("}");
            }
            else {
                StdOut.println(team + " is not eliminated");
            }
        }
    }
}