import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

public class Outcast {

    private final WordNet net;

    // constructor takes a WordNet object
    public Outcast(WordNet wordnet) {
        net = wordnet;
    }

    private void validateNouns(String[] nouns) {
        for (String noun : nouns) {
            if (noun == null || !net.isNoun(noun)) throw new IllegalArgumentException();
        }
    }

    // given an array of WordNet nouns, return an outcast
    public String outcast(String[] nouns) {
        validateNouns(nouns);
        String champion = null;
        int championDist = Integer.MIN_VALUE;
        for (int t = 0; t < nouns.length; t++) {
            int d = 0;
            for (int i = 0; i < nouns.length; i++) {
                d += net.distance(nouns[t], nouns[i]);
            }
            if (d > championDist) {
                champion = nouns[t];
                championDist = d;
            }
        }
        return champion;
    }
    
    // see test client below
    public static void main(String[] args) {
        WordNet wordnet = new WordNet(args[0], args[1]);
        Outcast outcast = new Outcast(wordnet);
        for (int t = 2; t < args.length; t++) {
            In in = new In(args[t]);
            String[] nouns = in.readAllStrings();
            StdOut.println(args[t] + ": " + outcast.outcast(nouns));
        }
    }
}