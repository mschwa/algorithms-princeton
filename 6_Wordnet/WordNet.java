import java.util.HashMap;
import edu.princeton.cs.algs4.SET;
import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.DirectedCycle;
import edu.princeton.cs.algs4.In;

public class WordNet {

    private final Digraph G;                              // digraph representation of wordnet
    private final HashMap<Integer, SET<String>> synsets;  // synsets[v] = synset associated with vertex v
    private final HashMap<String, SET<Integer>> vertsets; // vertsets[s] = all vertices whose synsets contain s
    private final SET<String> nouns;                      // all distinct nouns in net
    private final SAP sapCalculator;                      // sap queries calculator

    // constructor takes the name of the two input files
    public WordNet(String synsets, String hypernyms) {
        if (synsets == null || hypernyms == null) throw new IllegalArgumentException();
        this.synsets = new HashMap<>();
        vertsets = new HashMap<>();
        nouns = new SET<>();
        // parse synsets file
        parseSynsets(synsets);
        // initialize digraph based on number of read nouns
        G = new Digraph(nouns.size());
        // parse hypernyms file to create digraph edges
        parseHypernyms(hypernyms);
        // check if graph built is rooted DAG
        checkRootedDAG();
        // if digraph is valid, build SAP
        sapCalculator = new SAP(G);
    }

    private void parseSynsets(String synsets) {
        In synsetsTxt = new In(synsets);
        while (synsetsTxt.hasNextLine()) {
            // process line
            final String line = synsetsTxt.readLine();
            final String[] info = line.split(",");
            final int v = Integer.parseInt(info[0]);
            final String[] syns = info[1].split(" ");
            // update synsets and vertsets
            final SET<String> synonyms = new SET<>();
            for (String synonym : syns) {
                // add parsed synonym to synset in construction
                synonyms.add(synonym);
                // add synonym to set of all nouns in net
                nouns.add(synonym);
                // update vertset associated with current noun
                if (vertsets.containsKey(synonym)) {
                    vertsets.get(synonym).add(v);
                } else {
                    final SET<Integer> vertset = new SET<>();
                    vertset.add(v);
                    vertsets.put(synonym, vertset);
                }
            }
            // create mapping from vertex to synset
            this.synsets.put(v, synonyms);
        }
    }

    private void parseHypernyms(String hypernyms) {
        In hypernymsTxt = new In(hypernyms);
        while (hypernymsTxt.hasNextLine()) {
            // process line
            final String line = hypernymsTxt.readLine();
            final String[] info = line.split(",");
            final int v = Integer.parseInt(info[0]);
            // create digraph edges
            for (int i = 1; i < info.length; i++) {
                final int hyper = Integer.parseInt(info[i]);
                G.addEdge(v, hyper);
            }
        }
    }

    private void checkRootedDAG() {
        // check for cycles in digraph
        final DirectedCycle dc = new DirectedCycle(G);
        // check if input is rooted, that is, if there are vertices with no
        // outdegree (the ultimate hypernyms) and non-zero indegree (which
        // means they are in the net)
        int roots = 0;
        for (int i = 0; i < G.V(); i++) {
            if (G.outdegree(i) == 0 && G.indegree(i) > 0) roots++;
        }
        // throw exception if digraph is cyclic or if it has more than one root
        if (dc.hasCycle() || roots != 1) throw new IllegalArgumentException();
    }
 
    // // returns all WordNet nouns
    public Iterable<String> nouns() {
        final SET<String> copy = new SET<>();
        for (String noun : nouns)
            copy.add(noun);
        return copy;
    }
 
    // is the word a WordNet noun?
    public boolean isNoun(String word) {
        return nouns.contains(word);
    }

    // distance between nounA and nounB
    public int distance(String nounA, String nounB) {
        if (nounA == null || nounB == null) throw new IllegalArgumentException();
        if (!isNoun(nounA) || !isNoun(nounB)) throw new IllegalArgumentException();
        // get vertices associated with both nouns
        final SET<Integer> sourcesA = vertsets.get(nounA);
        final SET<Integer> sourcesB = vertsets.get(nounB);
        // return minimum set distance
        return sapCalculator.length(sourcesA, sourcesB);
    }
 
    // a synset (second field of synsets.txt) that is the common ancestor of nounA and nounB
    // in a shortest ancestral path (defined below)
    public String sap(String nounA, String nounB) {
        if (nounA == null || nounB == null) throw new IllegalArgumentException();
        if (!isNoun(nounA) || !isNoun(nounB)) throw new IllegalArgumentException();
        // get vertices associated with both nouns
        final SET<Integer> sourcesA = vertsets.get(nounA);
        final SET<Integer> sourcesB = vertsets.get(nounB);
        // find ancestor
        final int ancestor = sapCalculator.ancestor(sourcesA, sourcesB);
        // build string of nouns in ancestor synset
        String synset = "";
        final SET<String> syns = synsets.get(ancestor);
        int i = 0;
        for (String syn : syns) {
            // join synonyms separating them by space
            synset += syn;
            if (i < syns.size() - 1) synset += " ";
            i++;
        }
        return synset;
    }
 
    // do unit testing of this class
    public static void main(String[] args) {
        final WordNet net = new WordNet("synsets1.txt", "hypernyms1.txt");
        assert net.isNoun("two");
        assert !net.isNoun("twelve");
        assert net.distance("one", "three") == 1;
        assert net.distance("eleven", "zero") == 4;
        assert net.distance("seven", "eight") == 4;
        assert net.sap("three", "ten").equals("one");

        final WordNet invalidNet = new WordNet("https://coursera.cs.princeton.edu/algs4/assignments/wordnet/files/synsets3.txt",
                            "https://coursera.cs.princeton.edu/algs4/assignments/wordnet/files/hypernyms3InvalidTwoRoots.txt");

        System.out.println("\n** TESTS COMPLETED SUCCESSFULLY **");
    }
 }