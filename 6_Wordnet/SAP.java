import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.SET;
import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.In;

public class SAP {

    private final Digraph G;        // digraph on which ancestor queries are performed
    private SET<Integer> lastA;     // memoize last queried sets vertices for faster performance
    private SET<Integer> lastB;     // memoize last queried sets vertices for faster performance
    private int lastAncestor;       // memoirze ancestor found in last ancestor query
    private int lastLength;         // memoize length of last shortest ancestor path found

    // constructor takes the name of the two input files
    public SAP(Digraph G) {
        if (G == null) throw new IllegalArgumentException();
        // defensive copy of G
        this.G = new Digraph(G);
        lastA = new SET<>();
        lastB = new SET<>();
    }

    // length of shortest ancestral path between v and w; -1 if no such path
    public int length(int v, int w) {
        final int ancestor = ancestor(v, w);
        if (ancestor == -1) return -1;
        else return lastLength;
    }
   
    // a common ancestor of v and w that participates in a shortest ancestral path; -1 if no such path
    public int ancestor(int v, int w) {
       if (v < 0 || v >= G.V() || w < 0 || w >= G.V()) throw new IllegalArgumentException();
       // declare arrays contanining previous vertex visited for each vertex
       final int[] edgeToA = new int[G.V()];
       final int[] edgeToB = new int[G.V()];
       // put sources into arrays for use in generalized interlocking bfs
       final SET<Integer> sourcesA = new SET<>();
       final SET<Integer> sourcesB = new SET<>();
       sourcesA.add(v);
       sourcesB.add(w);
       // memoization
       if (!sourcesA.equals(lastA) || !sourcesB.equals(lastB)) {
           lastA = sourcesA;
           lastB = sourcesB;
           // perform interlocked bfs to find ancestor
           lastAncestor = bfs(edgeToA, sourcesA, edgeToB, sourcesB);
       }
       return lastAncestor;
    }
    
    private boolean isSetValid(Iterable<Integer> vertices) {
        // check input validity for generalized version of interlocking bfs
        if (vertices == null) return false;
        final int n = G.V();
        for (Integer v : vertices) {
            if (v == null) return false;
            if (v < 0 || v >= n) return false;
        }
        return true;
    }

    // length of shortest ancestral path between any vertex in v and any vertex in w; -1 if no such path
    public int length(Iterable<Integer> v, Iterable<Integer> w) {
        final int ancestor = ancestor(v, w);
        if (ancestor == -1) return -1;
        else return lastLength;
    }
    
    // a common ancestor that participates in shortest ancestral path; -1 if no such path
    public int ancestor(Iterable<Integer> v, Iterable<Integer> w) {
        if (!isSetValid(v) || !isSetValid(w)) throw new IllegalArgumentException();
        // declare arrays contanining previous vertex visited for each vertex
        final int[] edgeToA = new int[G.V()];
        final int[] edgeToB = new int[G.V()];
        // put sources into arrays for use in generalized interlocking bfs
        final SET<Integer> sourcesA = new SET<>();
        final SET<Integer> sourcesB = new SET<>();
        for (int x: v) sourcesA.add(x);
        for (int x: w) sourcesB.add(x);
        // return -1 for empty sets
        if ((sourcesA.size() == 0) && (sourcesB.size() == 0)) return -1;
       // memoization
       if (!sourcesA.equals(lastA) || !sourcesB.equals(lastB)) {
           lastA = sourcesA;
           lastB = sourcesB;
           // perform interlocked bfs to find ancestor
           lastAncestor = bfs(edgeToA, sourcesA, edgeToB, sourcesB);
       }
       return lastAncestor;
    }
        
    // interlocking, multi-source BFS for finding common ancestor in set generalization
    private int bfs(int[] edgeToA, Iterable<Integer> sourcesA,
                    int[] edgeToB, Iterable<Integer> sourcesB) {
        // declare array to store vertices visited by each BFS
        final boolean[] markedA = new boolean[G.V()];
        final boolean[] markedB = new boolean[G.V()];
        // declare array to store distances from each source
        final int[] distToA = new int[G.V()];
        final int[] distToB = new int[G.V()];
        for (int i = 0; i < G.V(); i++) {
            distToA[i] = Integer.MAX_VALUE;
            distToB[i] = Integer.MAX_VALUE;
        }
        // declare queues controlling order of each bfs
        final Queue<Integer> qA = new Queue<>();
        final Queue<Integer> qB = new Queue<>();
        // enqueue sources
        for (int source: sourcesA) {
            qA.enqueue(source);
            markedA[source] = true;
            distToA[source] = 0;
        }
        for (int source: sourcesB) {
            qB.enqueue(source);
            markedB[source] = true;
            distToB[source] = 0;
        }
        // initialize ancestor
        Integer ancestor = null;
        // start search
        while (!qA.isEmpty() || !qB.isEmpty()) {
            if (!qA.isEmpty()) {
                // dequeue next vertex
                final int v = qA.dequeue();
                // if other bfs has already visited this vertex, we
                // have found an ancestor
                if (markedB[v]) {
                    final int length = distToA[v] + distToB[v];
                    // store minimum distance found so far
                    if (ancestor == null || length < lastLength) {
                        lastLength = length;
                        ancestor = v;
                    }
                }
                // enqueue adjancent vertices
                for (int w: G.adj(v)) {
                    // expand search to vertices not yet visited
                    if (!markedA[w]) {
                        qA.enqueue(w);
                        markedA[w] = true;
                        edgeToA[w] = v;
                        distToA[w] = distToA[v] + 1;
                    }
                }
            }
            // same pass, but other search
            if (!qB.isEmpty()) {
                final int v = qB.dequeue();
                if (markedA[v]) {
                    final int length = distToA[v] + distToB[v];
                    if (ancestor == null || length < lastLength) {
                        lastLength = length;
                        ancestor = v;
                    }
                }
                for (int w: G.adj(v)) {
                    if (!markedB[w]) {
                        qB.enqueue(w);
                        markedB[w] = true;
                        edgeToB[w] = v;
                        distToB[w] = distToB[v] + 1;
                    }
                }
            }
        }
        // if no ancestor has been found
        if (ancestor == null) return -1;
        else return ancestor;
    }

    // do unit testing of this class
    public static void main(String[] args) {
        // build digraph
        In in = new In("https://coursera.cs.princeton.edu/algs4/assignments/wordnet/files/digraph1.txt");
        final Digraph G = new Digraph(in);
        // build SAP off of digraph
        final SAP sap = new SAP(G);
        assert sap.length(9, 2) == 4;
        System.out.println("** TESTS COMPLETED SUCESSFULLY **");
    }
}