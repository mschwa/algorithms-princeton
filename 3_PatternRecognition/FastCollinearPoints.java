/**
 * Class that uses sorting to find all segments contaning 4 or more collinear
 * points in a set. The overall procedure is as follows: given a point p
 * 
 *  1. Think of p of the origin;
 *  2. For each other point q, determine the slope it makes with p;
 *  3. Sort the points according to the slopes they make with p;
 *  4. Check if any 3 (or more) adjacent points in the sorted order have equal
 *     slopes with respect to p. If so, they are collinear with p.
 * 
 * The algorithm is fast because the bottleneck operation is sorting, which is
 * n * log(n) for each p and, thus, (n^2) * log(n) for the n points. Top-down
 * mergesort with minor imprevements is the chosen sorting algorithm.s
 */

import java.util.ArrayList;
import java.util.Comparator;
import edu.princeton.cs.algs4.StdIn;

public class FastCollinearPoints {

    private int segmentCount;                         // number of segments
    private final ArrayList<LineSegment> segments;    // starting points of segments containing 4 or more points
    private Comparator<Point> currComparator;         // store comparator locally

    // find all line segments containing 4 points.
    public FastCollinearPoints(Point[] points) {
        // perform basic validation of input and get defensive copy of input
        final Point[] input = validateInput(points);
        // intialize instance members
        segmentCount = 0;
        segments = new ArrayList<>();
        // if less than 4 points have been passed, there's nothing to do
        final int nPoints = input.length;
        if (nPoints < 4) return;
        // copy input once again to create auxiliary array modified by
        // mergesort
        final Point[] copy = new Point[input.length];
        for (int i = 0; i < nPoints; i++) { copy[i] = input[i]; }
        // iterate over all input points treating them as the current origin
        for (int p = 0; p < nPoints; p++) {
            // get slope comparator relative to current point
            currComparator = input[p].slopeOrder();
            // sort remaining points according to their slope relative to
            // the current origin
            final Point[] sorted = mergeSort(copy);
            // initialize count of collinear points (at two because two
            // points are always collinear)
            int collinearCount = 2;
            // initialize segment start and end points
            Point start = input[p];
            Point end = input[p];
            // if current point is smaller than start, it should be the start
            if (sorted[1].compareTo(input[p]) < 0) start = sorted[1];
            // if current point is greater than end, it should be the end
            if (sorted[1].compareTo(input[p]) > 0) end = sorted[1];
            // iterate over sorted array, skipping first element because that
            // is always the current origin (slope equal to negative infinity)
            for (int k = 2; k < nPoints; k++) {
                // check if next element in sorted array also belongs to
                // current segment
                final boolean collinear = currComparator.compare(sorted[k], sorted[k - 1]) == 0;
                // otherwise, restart segment count
                if (!collinear) {
                   // register current segment in case it has minimum
                   // length. To eliminate duplicates efficiently, only
                   // register segment when current point is the segment's
                   // start point. This elimates the need of comparing
                   // current segment to all segments previously registered
                    if (collinearCount >= 4 && input[p].compareTo(start) == 0) {
                        // registerSegment(start, end);
                        this.segments.add(new LineSegment(start, end));
                        segmentCount++;
                    }
                    // reset start and endpoints and start tracking new segment
                    start = input[p];
                    end = input[p];
                    collinearCount = 2;
                }
                // if current element is collinear
                else collinearCount++;

                // if current point is smaller than start, it should be the start
                if (sorted[k].compareTo(start) < 0) start = sorted[k];
                // if current point is greater than end, it should be the end
                if (sorted[k].compareTo(end) > 0) end = sorted[k];
            }
            // do one last check in case there's a segment in the last entries
            // of the sorted array
            if (collinearCount >= 4 && input[p].compareTo(start) == 0) {
                // registerSegment(start, end);
                this.segments.add(new LineSegment(start, end));
                segmentCount++;
            }
        }
    }

    private Point[] validateInput(Point[] points) {
        // check if constructor argument is null
        if (points == null) throw new IllegalArgumentException();
        // brute-force check if any point is null while creating input copy
        final Point[] copy = new Point[points.length];
        for (int i = 0; i < points.length; i++) {
            if (points[i] == null) throw new IllegalArgumentException();
            copy[i] = points[i];
        }
        // check if any point is repeated
        if (points.length > 1) {
            for (int i = 0; i < points.length - 1; i++) {
                for (int j = i + 1; j < points.length; j++) {
                    if (points[i].compareTo(points[j]) == 0) {
                        throw new IllegalArgumentException();
                    }
                } 
            }
        }
        return copy;
    }

    private boolean less(Point p1, Point p2) {
        // return whether p1 < p2
        return currComparator.compare(p1, p2) < 0;
    }

    private boolean isSorted(Point[] points, int lo, int hi) {
        if (hi == lo) return true;
        // checl whether all elements are greater than the element
        // immediatelly before them
        for (int i = lo; i < hi - 1; i++) {
            if (less(points[i+1], points[i])) return false;
        }
        return true;
    }

    private void merge(Point[] original, Point[] copy, int lo, int mid, int hi) {
        // check input arrays halves are sorted
        assert isSorted(original, lo, mid);
        assert isSorted(original, mid + 1, hi);
        // copy entire (unsorted) section of original array to corresponding
        // section of copy array
        for (int k = lo; k <= hi; k++) {
            copy[k] = original[k];
        }
        // intialize indexes for left and right halves in copy array respectively
        int i = lo;
        int j = mid + 1;
        // iterate over copied array halves producing the correct order in the
        // original array
        for (int k = lo; k <= hi; k++) {
            // if left half has been exhausted
            if (i > mid) original[k] = copy[j++];
            // if right half has been exhausted
            else if (j > hi) original[k] = copy[i++];
            // if current element in right half is strictly less than current
            // element in left half, insert it first
            else if (less(copy[j], copy[i])) original[k] = copy[j++];
            // otherwise, insert element from left half
            else original[k] = copy[i++];
        }
        // ensure merged section in original array is sorted
        assert isSorted(original, lo ,hi);
    }

    private void sort(Point[] original, Point[] copy, int lo, int hi) {
        // base case: sub-array of length 1 is already sorted
        if (lo == hi) return;
        // calculate halving point in array and recursively call sort
        // for halves
        final int mid = lo + (hi - lo) / 2;
        sort(original, copy, lo, mid);
        sort(original, copy,  mid + 1, hi);
        // if halves are already sorted relative to each other, no merge
        // is necessary
        if (less(original[mid], original[mid+1])) return;
        // merge halves starting from the base-case up
        merge(original, copy, lo, mid, hi);
    }

    private Point[] mergeSort(Point[] points) {
        // copy content of input array to auxiliary array
        final Point[] copy = new Point[points.length];
        for (int k = 0; k < points.length; k++) {
            copy[k] = points[k];
        }
        // trigger top-down mergesort
        sort(points, copy, 0, points.length - 1);

        // return sorted input array
        return points;
    }
    
    // return the number of line segments
    public int numberOfSegments() {
        return segmentCount;
    }

    // the line segments
    public LineSegment[] segments() {
        if (segmentCount == 0) {
            final LineSegment[] emptyArray = {};
            return emptyArray;
        } else {
            final LineSegment[] copy = new LineSegment[segmentCount];
            for (int i = 0; i < segmentCount; i++) {
                // segments[i] = new LineSegment(start[i], end[i]);
                copy[i] = segments.get(i);
            }
            return copy;
        }
    }

    public static void main(String[] args) {
        // declare points
        final Point p1 = new Point(0, 0);
        final Point p2 = new Point(4, 8);
        final Point p3 = new Point(-3, 1);
        final Point p4 = new Point(2, 4);
        final Point p5 = new Point(-6, 2);
        final Point p6 = new Point(-5, -10);
        final Point p7 = new Point(3, -1);
        final Point p8 = new Point(6, -2);
        final Point p9 = new Point(0, 1);
        final Point p10 = new Point(0, -1);
        final Point p11 = new Point(0, -7);

        final Point[] points = { p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11};
        // find segments
        final FastCollinearPoints result = new FastCollinearPoints(points);
        // check if right number of segments was found
        final int numberOfSegments = result.numberOfSegments();
        assert numberOfSegments == 3;   
        // check if right segments were found
        final LineSegment[] segments = result.segments();
        assert segments.length == numberOfSegments;
        assert segments[0].toString().equals("(-5, -10) -> (4, 8)");
        assert segments[1].toString().equals("(6, -2) -> (-6, 2)");
        assert segments[2].toString().equals("(0, -7) -> (0, 1)");
        // test invalid inputs
        try {
            // null points array
            new FastCollinearPoints(null);
            System.out.println("Threw first exception");
        } catch (IllegalArgumentException e) { ; }
        try {
            // null point
            new FastCollinearPoints(new Point[] {p1, null, p2} );
            System.out.println("Threw second exception");
        } catch (IllegalArgumentException e) { ; }
        try {
            // repeated point
            new FastCollinearPoints(new Point[] { p1, p2, p3, p4, p5, p1, p6, p7} );
            System.out.println("Threw third exception");
        } catch (IllegalArgumentException e) { ; }

        System.out.println("Passed all tests!");
    }

}