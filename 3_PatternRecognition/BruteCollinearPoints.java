/**
 * Brute force alogrithm for finding collinear points (no enhancements included
 * to improve its performance).
 */

import java.util.ArrayList;

public class BruteCollinearPoints {

    private int segmentCount;  // number of 4-point segments
    private final ArrayList<LineSegment> segments;     // segments containing 4 points

    // find all line segments containing 4 points.
    public BruteCollinearPoints(Point[] points) {
        // perform basic validation of input and get defensive copy of
        // original input
        final Point[] input = validateInput(points);
        // intialize instance members
        segmentCount = 0;
        segments = new ArrayList<>();
        // if less than 4 points have been passed, there's nothing to do
        final int nPoints = input.length;
        if (nPoints < 4) return;        
        // examine all unique 4-point combinations, one at a time
        for (int p = 0; p < nPoints - 3; p++) {
            for (int q = p + 1; q < nPoints - 2; q++) {
                // calculate slope relative to points[p]
                final double slope1 = input[p].slopeTo(input[q]);
                for (int r = q + 1; r < nPoints - 1; r++) {
                    // calculate slope relative to points[p] and if it is not
                    // equal to the previous slope, skip iteration
                    final double slope2 = input[p].slopeTo(input[r]);
                    if (slope1 != slope2) continue;
                    for (int s = r + 1; s < nPoints; s++) {
                        // calculate slope relative to points[p] and if it is not
                        // equal to the previous slope, skip iteration
                        final double slope3 = input[p].slopeTo(input[s]);
                        if (slope2 != slope3) continue;
                        
                        // if all slopes match, points are collinear. Then,
                        // find segment endpoins
                        Point start = input[p];
                        Point end = input[p];
                        // compare with remaining points to find actual segment ends
                        final Point[] remainingPoints = { input[q], input[r], input[s] };
                        for (Point pt: remainingPoints) {
                            // if point is smaller than current start
                            if (pt.compareTo(start) < 0) start = pt;
                            // if point is greater than current end
                            if (pt.compareTo(end) > 0) end = pt;
                        }
                        // register segment endpoints
                        this.segments.add(new LineSegment(start, end));
                        segmentCount++;
                    }
                }
            }
        }
    }

    private Point[] validateInput(Point[] points) {
        // check if constructor argument is null
        if (points == null) throw new IllegalArgumentException();
        // brute-force check if any point is null while creating input copy
        final Point[] copy = new Point[points.length];
        for (int i = 0; i < points.length; i++) {
            if (points[i] == null) throw new IllegalArgumentException();
            copy[i] = points[i];
        }
        // check if any point is repeated
        if (points.length > 1) {
            for (int i = 0; i < points.length - 1; i++) {
                for (int j = i + 1; j < points.length; j++) {
                    if (points[i].compareTo(points[j]) == 0) {
                        throw new IllegalArgumentException();
                    }
                } 
            }
        }
        return copy;
    }

    // return the number of line segments
    public int numberOfSegments() {
        return segmentCount;
    }

    // the line segments
    public LineSegment[] segments() {
        if (segmentCount == 0) {
            final LineSegment[] emptyArray = {};
            return emptyArray;
        } else {
            final LineSegment[] copy = new LineSegment[segmentCount];
            for (int i = 0; i < segmentCount; i++) {
                copy[i] = segments.get(i);
            }
            return copy;
        }
    }

    public static void main(String[] args) {
        // declare points
        final Point p1 = new Point(0, 0);
        final Point p2 = new Point(4, 8);
        final Point p3 = new Point(-3, 1);
        final Point p4 = new Point(2, 4);
        final Point p5 = new Point(-6, 2);
        final Point p6 = new Point(-5, -10);
        final Point p7 = new Point(3, -1);
        final Point[] points = { p1, p2, p3, p4, p5, p6, p7 };
        // find segments
        final BruteCollinearPoints result = new BruteCollinearPoints(points);
        // check if right number of segments was found
        final int numberOfSegments = result.numberOfSegments();
        assert numberOfSegments == 2;   
        // check if right segments were found
        final LineSegment[] segments = result.segments();
        assert segments.length == numberOfSegments;
        assert segments[0].toString().equals("(-5, -10) -> (4, 8)");
        assert segments[1].toString().equals("(3, -1) -> (-6, 2)");
        // test invalid inputs
        try {
            // null points array
            new BruteCollinearPoints(null);
            System.out.println("Threw first exception");
        } catch (IllegalArgumentException e) { ; }
        try {
            // null point
            new BruteCollinearPoints(new Point[] { p1, null, p2} );
            System.out.println("Threw second exception");
        } catch (IllegalArgumentException e) { ; }
        try {
            // repeated point
            new BruteCollinearPoints(new Point[] { p1, p2, p3, p4, p5, p1, p6, p7} );
            System.out.println("Threw third exception");
        } catch (IllegalArgumentException e) { ; }

        System.out.println("Passed all tests!");
    }

}