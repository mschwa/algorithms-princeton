/**
 * To avoid potential complications with integer overflow or floating-point
 * precision, it is assumed that the constructor arguments x and y are each
 * between 0 and 32,767. 
 */

import java.util.Comparator;
import edu.princeton.cs.algs4.StdDraw;

public class Point implements Comparable<Point> {

    private final int x;     // x-coordinate of this point
    private final int y;     // y-coordinate of this point

    /**
     * Initializes a new point.
     *
     * @param  x the <em>x</em>-coordinate of the point
     * @param  y the <em>y</em>-coordinate of the point
     */
    public Point(int x, int y) {
        /* DO NOT MODIFY */
        this.x = x;
        this.y = y;
    }

    /**
     * Draws this point to standard draw.
     */
    public void draw() {
        /* DO NOT MODIFY */
        StdDraw.point(x, y);
    }

    /**
     * Draws the line segment between this point and the specified point
     * to standard draw.
     *
     * @param that the other point
     */
    public void drawTo(Point that) {
        /* DO NOT MODIFY */
        StdDraw.line(this.x, this.y, that.x, that.y);
    }

    /**
     * Returns the slope between this point and the specified point.
     * Formally, if the two points are (x0, y0) and (x1, y1), then the slope
     * is (y1 - y0) / (x1 - x0). For completeness, the slope is defined to be
     * +0.0 if the line segment connecting the two points is horizontal;
     * Double.POSITIVE_INFINITY if the line segment is vertical;
     * and Double.NEGATIVE_INFINITY if (x0, y0) and (x1, y1) are equal.
     *
     * @param  that the other point
     * @return the slope between this point and the specified point
     */
    public double slopeTo(Point that) {
        // handle edge cases
        if (this.x == that.x) {
            // if points are equal
            if (this.y == that.y) return Double.NEGATIVE_INFINITY;
            // if points are in same vertical line
            return Double.POSITIVE_INFINITY;
        } else if (this.y == that.y) {
            return +0;
        } else {
            return (double) (that.y - this.y) / (that.x - this.x);
        }
    }

    /**
     * Compares two points by y-coordinate, breaking ties by x-coordinate.
     * Formally, the invoking point (x0, y0) is less than the argument point
     * (x1, y1) if and only if either y0 < y1 or if y0 = y1 and x0 < x1.
     *
     * @param  that the other point
     * @return the value <tt>0</tt> if this point is equal to the argument
     *         point (x0 = x1 and y0 = y1);
     *         a negative integer if this point is less than the argument
     *         point; and a positive integer if this point is greater than the
     *         argument point
     */
    public int compareTo(Point that) {
        if (this.y < that.y) return -1;
        if (this.y > that.y) return +1;
        if (this.y == that.y) {
            if (this.x < that.x) return -1;
            if (this.x > that.x) return +1;
        }
        return 0;
    }

    private class SlopeOrder implements Comparator<Point> {
        public int compare(Point p1, Point p2) {
            // calculate slopes to each point
            final double s1 = Point.this.slopeTo(p1);
            final double s2 = Point.this.slopeTo(p2);
            // return comparison result
            if (s1 < s2) return -1;
            if (s1 > s2) return +1;
            return 0;
        }
    }

    /**
     * Compares two points by the slope they make with this point.
     * The slope is defined as in the slopeTo() method.
     *
     * @return the Comparator that defines this ordering on points
     */
    public Comparator<Point> slopeOrder() {
        /* YOUR CODE HERE */
        return new SlopeOrder();
    }


    /**
     * Returns a string representation of this point.
     * This method is provide for debugging;
     * your program should not rely on the format of the string representation.
     *
     * @return a string representation of this point
     */
    public String toString() {
        /* DO NOT MODIFY */
        return "(" + x + ", " + y + ")";
    }

    /**
     * Unit tests the Point data type.
     */
    public static void main(String[] args) {
        final Point p0 = new Point(0, 0);
        final Point p1 = new Point(2, 1);
        final Point p2 = new Point(-1, 4);
        final Point p3 = new Point(2, -6);
        final Point p4 = new Point(-1, 0);
        final Point p5 = new Point(1, 0);
        // test compareTo behavior
        assert p0.compareTo(p1) == -1;
        assert p0.compareTo(p2) == -1;
        assert p0.compareTo(p3) == +1;
        assert p0.compareTo(p4) == +1;
        assert p0.compareTo(p5) == -1;
        assert p0.compareTo(p0) == 0;
        // test slopeTo method
        assert p0.slopeTo(p1) == 1.0 / 2;
        assert p0.slopeTo(p2) == -4;
        assert p0.slopeTo(p3) == -3;
        assert p0.slopeTo(p4) == 0;
        assert p0.slopeTo(p5) == 0;
        assert p0.slopeTo(p0) == Double.NEGATIVE_INFINITY;
        assert p1.slopeTo(p3) == Double.POSITIVE_INFINITY;
        assert p2.slopeTo(p3) == -10.0 / 3;
        assert p2.slopeTo(p3) == p3.slopeTo(p2);
        // test comparator
        final Comparator<Point> comp0 = p0.slopeOrder();
        final Comparator<Point> comp1 = p1.slopeOrder();
        assert comp0.compare(p1, p2) == +1;
        assert comp0.compare(p2, p1) == -1;
        assert comp0.compare(p1, p1) == 0;
        assert comp0.compare(p4, p3) == +1;
        assert comp0.compare(p4, p5) == 0;
        assert comp0.compare(p0, p2) == -1;
        assert comp1.compare(p3, p1) == +1;
        assert comp1.compare(p0, p3) == -1;
        System.out.println("** PASSED ALL TESTS **");
    }
}
