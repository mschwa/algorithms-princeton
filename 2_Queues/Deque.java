/**
 * Performance requirements. this deque implementation must support each deque
 * operation (including construction) in constant worst-case time. A deque
 * containing n items must use at most 48n + 192 bytes of memory. Additionally,
 * your iterator implementation must support each operation (including
 * construction) in constant worst-case time. 
 * 
 * Requirements satisfaction: because this implementation must be worst-case
 * constant time, a linked-list representation is chosen. Each node is 48
 * bytes: 8 bytes from node inner class extra overhead, 8 bytes from reference
 * to Item, 8 bytes from reference to next node and 8 bytes from reference to
 * previous node. The remaining bytes of memory come from the outer class.
 * 
 * @param <Item> generic type of items stored in Deque
 *
 *******************************************************************************/

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item> {

    private Node<Item> first;
    private Node<Item> last;
    private int size;

    private static class Node<Item> {
        private Item item;
        private Node<Item> next;
        private Node<Item> prev;
    }

    // construct an empty deque
    public Deque() {
        first = null;
        last = null;
        size = 0;
    }

    // is the deque empty?
    public boolean isEmpty() {
        return size == 0;
    }

    // return the number of items on the deque
    public int size() {
        return size;
    }

    // add the item to the front
    public void addFirst(Item item) {
        // validate input
        if (item == null) throw new IllegalArgumentException();
        // edge case: deque is empty
        if (size == 0) {
            first = new Node<Item>();
            first.item = item;
            last = first;
        } else {
            // save reference to old first node
            final Node<Item> oldFirst = first;
            // create and initialize new first node
            first = new Node<Item>();
            first.item = item;
            first.next = oldFirst;
            // update old first's previous reference
            oldFirst.prev = first;
        }
        // update size
        size++;
    }

    // add the item to the back
    public void addLast(Item item) {
        // validate input
        if (item == null) throw new IllegalArgumentException();
        // edge case: last is null
        if (size == 0) {
            last = new Node<Item>();
            last.item = item;
            first = last;
        } else {
            // save reference to old last node
            final Node<Item> oldLast = last;
            // create and initialize new last node
            last = new Node<Item>();
            last.item = item;
            last.prev = oldLast;
            // update old last's next reference
            oldLast.next = last;
        }
        // update size
        size++;
    }

    // remove and return the item from the front
    public Item removeFirst() {
        // validate input
        if (size == 0) throw new NoSuchElementException();
        // keep value stored in node
        final Item oldFirstItem = first.item;
        // remove reference to old first node in new first node
        if (size > 1) {
            first.next.prev = null;
            // delete old first node
            first = first.next;
        } else {
            // if last element is being removed from deque
            first = null;
            last = null;
        }
        // update size
        size--;

        return oldFirstItem;
    }

    // remove and return the item from the back
    public Item removeLast() {
        // validate input
        if (size == 0) throw new NoSuchElementException();
        // keep value stored in node
        final Item oldLastItem = last.item;
        // remove reference to old last node in new last node
        if (size > 1) {
            last.prev.next = null;
            // delete old last node
            last = last.prev;
        } else {
            // if last element is being removed from deque
            first = null;
            last = null;
        }
        // update size
        size--;

        return oldLastItem;
    }

    private class DequeIterator implements Iterator<Item> {
        private Node<Item> current;

        public DequeIterator(Node<Item> current) {
            this.current = current;
        }

        public boolean hasNext() { return current != null; }

        public Item next() {
            // prevent invalid call of method
            if (!hasNext()) throw new NoSuchElementException();
            // return item stored by current node and advance it
            final Item oldItem = current.item;
            current = current.next;
            return oldItem;
        }

        public void remove() { throw new UnsupportedOperationException(); }
    }

    // return an iterator over items in order from front to back
    public Iterator<Item> iterator() {
        return new DequeIterator(first);
    }

    // unit testing (required)
    public static void main(String[] args) {
        final Deque<Integer> deque = new Deque<>();
        // addFirst in empty deque
        deque.addFirst(1);
        assert !deque.isEmpty();
        assert deque.size == 1;
        // removeFirst to empty deque
        final int f1 = deque.removeFirst();
        assert f1 == 1;
        assert deque.isEmpty();
        assert deque.size() == 0;
        // removeLast should also work
        deque.addFirst(1);
        final int f2 = deque.removeLast();
        assert f2 == 1;
        assert deque.isEmpty();
        assert deque.size() == 0;
        // repeat tests for addLast
        deque.addLast(1);
        assert !deque.isEmpty();
        assert deque.size == 1;
        // removeLast to empty deque
        final int f3 = deque.removeLast();
        assert f3 == 1;
        assert deque.isEmpty();
        assert deque.size() == 0;
        // removeFirst should also work
        deque.addFirst(1);
        final int f4 = deque.removeFirst();
        assert f4 == 1;
        assert deque.isEmpty();
        assert deque.size() == 0;
        
        // test multiple consecutive addFirst and removeLast calls
        for (int i = 0; i < 5; i++) {
            deque.addFirst(i);
            assert deque.size() == i + 1;
            assert !deque.isEmpty();
        }
        for (int i = 0; i < 5; i++) {
            assert i == deque.removeLast();
            assert deque.size() == 4 - i;
            if (i == 4) assert deque.isEmpty();
            else assert !deque.isEmpty();
        }
         // test multiple consecutive addLast and removeFirst calls
         for (int i = 0; i < 5; i++) {
            deque.addLast(i);
            assert deque.size() == i + 1;
            assert !deque.isEmpty();
        }
        for (int i = 0; i < 5; i++) {
            assert i == deque.removeFirst();
            assert deque.size() == 4 - i;
            if (i == 4) assert deque.isEmpty();
            else assert !deque.isEmpty();
        }
        // mix addFirst with addLast and removeFirst with removeLast
        deque.addFirst(3);
        deque.addLast(4);
        deque.addFirst(2);
        deque.addLast(5);
        deque.addFirst(1);
        assert deque.removeLast() == 5;
        assert deque.removeFirst() == 1;
        assert deque.removeLast() == 4;
        assert deque.removeFirst() == 2;
        assert deque.removeLast() == 3;

        // test iterator
        for (int i = 0; i < 5; i++) deque.addLast(i);
        int val = 0;
        for (int element : deque) {
            assert element == val;
            val++;
        }
        assert val == 5;
        // empty list
        for (int i = 0; i < 5; i++) deque.removeLast();
        assert deque.isEmpty();

        // test exceptions
        try { 
            deque.addFirst(null);
            System.out.println("Failed at 1st exception");
        }
        catch (IllegalArgumentException e) { ; }
        // --
        try { 
            deque.addLast(null); 
            System.out.println("Failed at 2nd exception");
        }
        catch (IllegalArgumentException e) { ; }
        // --
        try {
            deque.removeFirst();
            System.out.println("Failed at 3rd exception");
        }
        catch (NoSuchElementException e) { ; }
        // --
        try {
            deque.removeLast();
            System.out.println("Failed at 4th exception");
        }
        catch (NoSuchElementException e) { ; }
        // --
        final Iterator<Integer> it = deque.iterator();
        try {
            it.next();
            System.out.println("Failed at 5th exception");
        }
        catch (NoSuchElementException e) { ; }
        // --
        try {
            it.remove();
            System.out.println("Failed at 6th exception");
        }
        catch (UnsupportedOperationException e) { ; }

        System.out.println(" ** TESTS COMPLETED SUCCESSFULLY **");

    }

}