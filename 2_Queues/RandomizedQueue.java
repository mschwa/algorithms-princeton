/**
 * A randomized queue is a collection similar to a stack or queue, except
 * that the item removed is chosen uniformly at random among items in the
 * data structure.
 * 
 * Performance requirements. Your randomized queue implementation must
 * support each randomized queue operation (besides creating an iterator)
 * in constant amortized time. That is, any intermixed sequence of m
 * randomized queue operations (starting from an empty queue) must take at
 * most cm steps in the worst case, for some constant c. A randomized queue
 * containing n items must use at most 48n + 192 bytes of memory.
 * Additionally, your iterator implementation must support operations next()
 * and hasNext() in constant worst-case time; and construction in linear time;
 * you may (and will need to) use a linear amount of extra memory per iterator.
 * 
 * This implementation uses one resizing-array to enqueue items as done in a
 * regular queue (and having filled spots contiguous in the underlying array).
 * The difference is in the dequeue operation. For that, we select a random
 * item from the queue, store a reference to it, then put the last element in
 * in its place. This change of order is fine because, in the RandomizedQueue,
 * dequeueing is done randomly, so there's no need to enforce a specific order
 * in the queue items.
 */

import java.util.Iterator;
import java.util.NoSuchElementException;
import edu.princeton.cs.algs4.StdRandom;

public class RandomizedQueue<Item> implements Iterable<Item> {

    private Item[] q;   // items
    private int size;   // number of elements in queue
    private int first;  // index of first item
    private int last;   // index of next available slot

    // construct an empty randomized queue
    public RandomizedQueue() {
        q = (Item[]) new Object[2];
        first = 0;
        last = 0;
        size = 0;        
    }

    // is the randomized queue empty?
    public boolean isEmpty() {
        return size == 0;
    }

    // return the number of items on the randomized queue
    public int size() {
        return size;
    }

    // resize array to new specified size
    private void resize(int newSize) {
        // declare new array
        final Item[] resized = (Item[]) new Object[newSize];
        // initialize it starting new queue at 0th index
        for (int i = 0; i < size; i++) {
            resized[i] = q[(first + i) % q.length];
        }
        // perform required member updates
        q = resized;
        first = 0;
        last = size;
    }

    // add the item
    public void enqueue(Item item) {
        // validate argument
        if (item == null) throw new IllegalArgumentException();
        // resize array if necessary
        if (size == q.length) resize(2 * size);
        // enqueue item
        q[last++] = item;
        // wrap around if necessary and update item count
        last = last % q.length;
        size++;
    }


    private int randomIndex() {
        final int index = StdRandom.uniform(0, size);
        return (first + index) % q.length;
    }

    // remove and return a random item
    public Item dequeue() {
        // validate dequeue operation
        if (isEmpty()) throw new NoSuchElementException();
        // resize array if necessary
        if (size == q.length / 4) resize(q.length / 2);
        // get index of element to be dequeued
        final int index = randomIndex();
        // save reference to item
        final Item item = q[index];
        // calculate position of last element enqueued (should be last - 1,
        // but we must account for wrapping)
        last = (q.length + last - 1) % q.length;
        // put last element at index and make last position null, so that next
        // enqueued element can be inserted there and loitering is avoided
        q[index] = q[last];
        q[last] = null;
        // wrap around if necessary and update item count
        size--;
        
        return item;
    }

    // return a random item (but do not remove it)
    public Item sample() {
        // validate sample operation
        if (isEmpty()) throw new NoSuchElementException();
        // retrieve random item
        return q[randomIndex()];
    }

    private class RandomizedQueueIterator implements Iterator<Item> {
        
        private int current = 0;    // current position in iteration
        private final Item[] shuffled;    // shuffled array for iteration

        public RandomizedQueueIterator() {
            // create copy of queue where first is at index 0
            shuffled = (Item[]) new Object[size];
            for (int i = 0; i < size; i++) {
                shuffled[i] = q[(first + i) % q.length];
            }
            StdRandom.shuffle(shuffled);
        }

        public boolean hasNext() { return current < size; }

        public Item next() {
            if (current == size) throw new NoSuchElementException();
            return shuffled[current++];
        }

        public void remove() { throw new UnsupportedOperationException(); }
    }

    // return an independent iterator over items in random order
    public Iterator<Item> iterator() {
        return new RandomizedQueueIterator();
    }

    // unit testing (required)
    public static void main(String[] args) {
        System.out.println("\nTesting...");
        // instantiate RandomizedQueue and begin tests
        final RandomizedQueue<Integer> q = new RandomizedQueue<>();
        assert q.isEmpty();
        // add items to queue
        for (int i = 0; i < 5; i++) { q.enqueue(i); }
        assert !q.isEmpty();
        assert q.size() == 5;
        // iterate
        int itemCount = 0;
        for (int i: q) { itemCount++; }
        assert itemCount == 5;
        // sample item
        final int s1 = q.sample();
        assert s1 >= 0;
        assert s1 < 5;
        assert !q.isEmpty();
        assert q.size() == 5;
        // dequeue item
        final int d1 = q.dequeue();
        assert d1 >= 0;
        assert d1 < 5;
        assert !q.isEmpty();
        assert q.size() == 4;
        // verify iterators are independently generated
        final Iterator<Integer> it1 = q.iterator();
        final Iterator<Integer> it2 = q.iterator();
        final int[] items1 = new int[q.size()];
        final int[] items2 = new int[q.size()];
        boolean allMatch = true;
        while (it1.hasNext()) {
            if (it1.next() != it2.next()) {
                allMatch = false;
                break;
            }
        }
        assert !allMatch;
        assert q.size() == 4;
        // dequeue remaining items
        for (int i = 0; i < 4; i++) {
            q.dequeue();
            assert q.size() == 3 - i;
        }
        assert q.isEmpty();

        // test exceptions
        try { 
            q.enqueue(null);
            System.out.println("Failed at 1st exception");
        }
        catch (IllegalArgumentException e) { ; }
        // --
        try { 
            q.sample(); 
            System.out.println("Failed at 2nd exception");
        }
        catch (NoSuchElementException e) { ; }
        // --
        try {
            q.dequeue();
            System.out.println("Failed at 3rd exception");
        }
        catch (NoSuchElementException e) { ; }
        // --
        final Iterator<Integer> it = q.iterator();
        try {
            it.next();
            System.out.println("Failed at 4th exception");
        }
        catch (NoSuchElementException e) { ; }
        // --
        try {
            it.remove();
            System.out.println("Failed at 4th exception");
        }
        catch (UnsupportedOperationException e) { ; }
       

        System.out.println("** SUCCESSFULLY RAN ALL TESTS **");
    }

}