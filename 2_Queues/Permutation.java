/**
 * The Permutation class takes an integer k as a command-line argument; reads
 * a sequence of strings from standard input using StdIn.readString(); and
 * prints exactly k of them, uniformly at random. Each item from the sequence
 * is printed at most once.
 * 
 * In order to get extra credit, the maximum size of the RandomizedQueue used
 * to store the input strings is no greater than k. The uniform distribution
 * over the input strings is achieved via the a simple version of the reservoir
 * sampling algoritm (https://en.wikipedia.org/wiki/Reservoir_sampling). The
 * simple version is explained below:
 * 
 * The algorithm works by maintaining a reservoir of size k, which initially
 * contains the first k items of the input. It then iterates over the remaining
 * items until the input is exhausted. 
 * 
 * Let i >= k be the index of the item currently under consideration. The
 * algorithm then generates a random number r in [0, i). If r < k, then the
 * item is selected and replaces whichever item currently occupies the r-th
 * position in the reservoir. Otherwise, the item is discarded.
 * 
 * In effect, for all i, the ith element of the input is chosen to be included
 * in the reservoir with probability k / i. Similarly, given that r < k, the
 * probability of any of the k elements in the reservoir to be replaced (i.e
 * be the in the r-th position) is (1 / k). Therefore, at each iteration the
 * probability that the r-th element of the reservoir array is chosen to be
 * replaced with probability (1 / k) × (k / i) = 1 / i. It can be shown that
 * when the algorithm has finished executing, each item in the input population
 * has equal probability (i.e., k / n) of being chosen for the reservoir. 
 */

import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdRandom;

public class Permutation {

    public static void main(String[] args) {
        // read k from command-line
        final int k = Integer.parseInt(args[0]);
        // check k >= 0
        if (k < 0) throw new IllegalArgumentException("k must be greater than or equal to 0");
        if (k == 0) return;
        // declare RandomizedQueue object to store input strings
        final RandomizedQueue<String> q = new RandomizedQueue<>();
        // declare counter of input strings
        int strCount = 0;
        // read strings and store at most k of them
        while (!StdIn.isEmpty()) {
            final String s = StdIn.readString();
            strCount++;
            // if size < k, enqueue item, otherwise randomly dequeue item
            // and replace it by new one
            if (q.size() < k) q.enqueue(s);
            else if (StdRandom.uniform() < (double) k / strCount) {
                // k / strCount represents the chance of one of the first k
                // items being in the final k selected group.
                q.dequeue();
                q.enqueue(s);
            }            
        } 
        // print k items
        for (String s : q) { System.out.println(s); }

        
    }
}