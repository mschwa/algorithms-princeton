import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;
import edu.princeton.cs.algs4.Quick3way;

public class BurrowsWheeler {
    private static final int R = 256;
    
    // apply Burrows-Wheeler transform, reading from standard input and
    // writing to standard output 
    public static void transform() {
        // read byte stream and build circular suffix array from it
        final String input = BinaryStdIn.readString();
        final CircularSuffixArray ca = new CircularSuffixArray(input);
        final int len = ca.length();
        final char[] encoding = new char[len];
        // initialize variable storing row in which original string appears
        // in Burrows-Wheeler matrix
        int first = -1;
        // build BW encoding t[] using the fact that if a circular suffix
        // starts at index[i] of the input string, then the last character of
        // that suffix must be the character preceding it in the original
        // string: t[i] = input[index[i] - 1]
        for (int i = 0; i < len; i++) {
            // get index where suffix starts in origial string
            final int idx = ca.index(i);
            // get index of character preceding it
            final int pos = (idx == 0) ? len - 1 : idx - 1;
            encoding[i] = input.charAt(pos);
            if (idx == 0) first = i;
        }
        // write transform
        BinaryStdOut.write(first);
        for (char symbol: encoding) 
            BinaryStdOut.write(symbol);

        // close streams
        BinaryStdIn.close();
        BinaryStdOut.close();
    }

    private static int[] buildNext(char[] f, char[] t) {
        final int[] next = new int[t.length];
        final int[] count = new int[R + 1];
        // create cumulates array for all chars present in transform
        for (int symbol: f)
            count[symbol + 1]++;
        for (int r = 0; r < R; r++)
            count[r + 1] += count[r];
        // iterate over the transform t[], get element in current position
        // and find where it appears in sorted array from the cumulates array.
        // This avoids possibly-quadratic complexity of retrieving the item
        // directly from the sorted array.
        for (int c = 0; c < t.length; c++) {
            // get current transform char (last char in cth row)
            final int symbol = t[c];
            // get its corresponding position in sorted column (row in which
            // it is the first char)
            final int pos = count[symbol]++;
            // the next suffix generated after symbol is in the first position
            // is that in the row where symbol is in the last position (current
            // row in transform)
            next[pos] = c;
        }
        return next;
    }

    // apply Burrows-Wheeler inverse transform,
    // reading from standard input and writing to standard output
    public static void inverseTransform() {
        // parse input
        final int first = BinaryStdIn.readInt();
        final char[] t = BinaryStdIn.readString().toCharArray();
        // declare f array where sorted symbols in transform will be stored
        // and count array for key-indexed counting
        final char[] f = new char[t.length];
        final int[] count = new int[R + 1];
        // use key-indexed counting to get first column of BWM (consiting of
        // sorted symbols in transform)
        for (int symbol: t)
            count[symbol + 1]++;
        for (int r = 0; r < R; r++)
            count[r + 1] += count[r];
        for (int i = 0; i < t.length; i++) {
            final int idx = count[t[i]]++;
            f[idx] = t[i];
        }
        // construct next array: If the jth original suffix (original string,
        // shifted j characters to the left) is the ith row in the sorted
        // order (f), next[i] is the row in the sorted order where the
        // (j + 1)st original suffix appears.
        final int[] next = buildNext(f, t);
        // recover original string using next and the sorted column
        int curr = first;
        for (int i = 0; i < t.length; i++) {
            BinaryStdOut.write(f[curr]);
            curr = next[curr];
        }
        // close streams
        BinaryStdIn.close();
        BinaryStdOut.close();
    }


    // if args[0] is "-", apply Burrows-Wheeler transform
    // if args[0] is "+", apply Burrows-Wheeler inverse transform
    public static void main(String[] args) {
        if (args.length == 0) return;
        if      (args[0].equals("-")) transform();
        else if (args[0].equals("+")) inverseTransform();
    }
}