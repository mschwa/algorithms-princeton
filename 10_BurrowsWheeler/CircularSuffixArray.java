import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Quick3way;

import java.util.Arrays;
import java.util.Comparator;

public class CircularSuffixArray {
    private final String s;
    private final Integer[] index;

    // circular suffix array of s
    public CircularSuffixArray(String s) {
        if (s == null) throw new IllegalArgumentException();
        this.s = s;
        // declare array storing starting index of each circular suffix in
        // original string
        index = new Integer[s.length()];
        for (int i = 0; i < s.length(); i++)
            index[i] = i;
        // sort indices based on the suffixes the index into. Done via
        // comparator for memory savings
        final SuffixOrder comp = new SuffixOrder();
        Arrays.sort(index, comp);
    }

    // length of s
    public int length() {
        return s.length();
    }

    // returns index of ith sorted suffix
    public int index(int i) {
        if (i <  0 || i >= s.length()) throw new IllegalArgumentException();
        return index[i];
    }

    private class SuffixOrder implements Comparator<Integer> {
        // compare two indices based on the suffix they represent
        public int compare(Integer self, Integer other) {
            final int len = s.length();
            int m = self;
            int n = other;
            // do comparison within loop to seamlessly handle case where
            // first character of each suffix (and possibly others) is the
            //same. Allow comparisons up to len because rotations can be only
            // be as long as original string
            for (int i = 0; i < len; i++) {
                m = (self + i) % len;
                n = (other + i) % len;
                int cmp = s.charAt(m) - s.charAt(n);
                if (cmp != 0) return cmp;
            }
            return 0;
        }
    }

    // unit testing (required)
    public static void main(String[] args) {
        In in = new In(args[0]);
        String text = in.readAll();
        final CircularSuffixArray ca = new CircularSuffixArray(text);
        assert ca.length() == 12;
        final int[] expectedIndices = {11, 10, 7, 0, 3, 5, 8, 1, 4, 6, 9, 2};
        for (int i = 0; i < text.length(); i++) {
            assert ca.index(i) == expectedIndices[i];
        }
    }

}