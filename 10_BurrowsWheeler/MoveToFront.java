import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;

public class MoveToFront {
    private static final int R = 256; 

    // initialize array with alphabet in lexicographical order
    private static char[] initializeSequence() {
        final char[] seq = new char[R];
        for (int rank = 0; rank < R; rank++) {
            seq[rank] = (char) rank;
        }
        return seq;
    }

    // apply move-to-front encoding, reading from standard input and writing to standard output
    public static void encode() {
        final char[] seq = initializeSequence();
        while (!BinaryStdIn.isEmpty()) {
            final char symbol = BinaryStdIn.readChar();
            // if symbol is first item in array, there's no exchange to be done
            if (seq[0] == symbol) {
                BinaryStdOut.write(0, 8);
                continue;
            }
            // if symbol is not in first position, declare cursors pointing to
            // elements visited in current and previous iterations
            char prev = seq[0];
            char curr;
            for (int i = 1; i < R; i++) {
                // keep value of current element
                curr = seq[i];
                // move previous element to current position
                seq[i] = prev;
                // update previous cursor
                prev = curr;
                // if symbol was originally in current position
                if (curr == symbol) {
                    // move to front
                    seq[0] = symbol;
                    BinaryStdOut.write(i, 8);
                    break;
                }
            }
        }
        BinaryStdIn.close();
        BinaryStdOut.close();
    }

    // apply move-to-front decoding, reading from standard input and writing to standard output
    public static void decode() {
        final char[] seq = initializeSequence();
        while (!BinaryStdIn.isEmpty()) {
            final int pos = BinaryStdIn.readInt(8);
            // if symbol is at first position, there's no exchange to be done
            if (pos == 0) {
                BinaryStdOut.write(seq[0]);
                continue;
            }
            // if symbol is not in first position, declare cursors pointing to
            // elements visited in current and previous iterations
            char prev = seq[0];
            char curr = seq[1];
            for (int i = 1; i <= pos; i++) {
                // keep value of current element
                curr = seq[i];
                // move previous element to current position
                seq[i] = prev;
                // update previous cursor
                prev = curr;
            }
            BinaryStdOut.write(curr);
            // move to front
            seq[0] = curr;
        }
        BinaryStdIn.close();
        BinaryStdOut.close();

    }

    // if args[0] is "-", apply move-to-front encoding
    // if args[0] is "+", apply move-to-front decoding
    public static void main(String[] args) {
        if (args.length == 0) return;
        if      (args[0].equals("-")) encode();
        else if (args[0].equals("+")) decode();
    }

}