import java.awt.Color;
import edu.princeton.cs.algs4.Picture;

public class SeamCarver {

    private int width;
    private int height;
    private Picture pic;

    // create a seam carver object based on the given picture
    public SeamCarver(Picture picture) {
        if (picture == null) throw new IllegalArgumentException();
        pic = new Picture(picture);
        // memoize current height and width for faster retrievals
        width = pic.width();
        height = pic.height();     
    }
 
    // current picture
    public Picture picture() {
        return new Picture(pic);
    }
 
    // width of current picture
    public int width() {
        return width;
    }
 
    // height of current picture
    public int height() {
        return height;
    }

    private void validatePixel(int x, int y) {
        if (x < 0 || x > width - 1 || y < 0 || y > height - 1) throw new IllegalArgumentException();
    }
 
    // energy of pixel at column x and row y
    public double energy(int x, int y) {
        validatePixel(x, y);
        // border pixels have engergy of 1000
        if (x == 0 || y == 0 || x == width - 1 || y == height - 1) {
            return 1000;
        }
        // create references to neighbouring pixels
        final Color right = pic.get(x + 1, y);
        final Color left = pic.get(x - 1, y);
        final Color bottom = pic.get(x, y + 1);
        final Color top = pic.get(x, y - 1);
        // calculate x central differences
        final int rx = right.getRed() - left.getRed();
        final int gx = right.getGreen() - left.getGreen();
        final int bx = right.getBlue() - left.getBlue();
        // calculate y central differences
        final int ry = bottom.getRed() - top.getRed();
        final int gy = bottom.getGreen() - top.getGreen();
        final int by = bottom.getBlue() - top.getBlue();
        // calculate pixel energy
        return Math.sqrt((rx * rx) + (gx * gx) + (bx * bx) +
                         (ry * ry) + (gy * gy) + (by * by));
    }

    private int[] reachableRows(int row) {
        if (row == 0) {
            return new int[] { row, row + 1 };
        } else if (row == height - 1) {
            return new int[] { row - 1, row };
        } else {
            return new int[] { row -1, row, row + 1 };
        }
    }
 
    // sequence of indices for horizontal seam
    public int[] findHorizontalSeam() {
        // handle case where there's only one image row left
        if (height == 1) {
            final int[] rows = new int[width];
            for (int i = 0; i < width; i++) rows[i] = 0;
            return rows;
        }
        // we only have to keep two arrays in memory for iteration: one
        // containing the total energy for each pixel in the current row,
        // and another for relaxation of the following row. Also define
        // the colTo matrix to store the column visited to before getting
        // to a pixel when following the lowest energy path
        double[] energyToCurr = new double[height];
        double[] energyToNext = new double[height];
        final int[][] rowTo = new int[width][height];
         // initialize the energy arrays and the first row of rowTo
         for (int row = 0; row < height; row++) {
            energyToCurr[row] = 1000;
            energyToNext[row] = Double.POSITIVE_INFINITY;
            rowTo[0][row] = row;
        }
        // declare variables for holding lowest energy champion and the
        // row where its last pixel is found
        double championEnergy = Double.POSITIVE_INFINITY;
        int championRow = -1;
        // process arrays in ascending column order. Because pixels can only
        // reach other pixels to their right, this performs all necessary
        // relaxations in one pass and thus has the same effect as topological
        // sorting in explicit edge-weighted graph implementations
        for (int col = 0; col < width; col++) {
            if (col < width - 1) {
                // memoize energy calculations for adjacent column
                final double[] adjEnergies = new double[height];
                for (int row = 0; row < height; row++) {
                    adjEnergies[row] = energy(col + 1, row);
                }
                for (int row = 0; row < height; row++) {
                // if col is not rightmost column
                    // get rows reachable from current pixel
                    final int[] adjRows = reachableRows(row);
                    for (int i = 0; i < adjRows.length; i++) {
                        // get reachable row
                        final int adjRow = adjRows[i];
                        // relax reachable pixel in just retrieved row
                        if (energyToNext[adjRow] > energyToCurr[row] + adjEnergies[adjRow]) {
                            energyToNext[adjRow] = energyToCurr[row] + adjEnergies[adjRow];
                            rowTo[col + 1][adjRow] = row;
                        }
                    }
                }
                // advance iteration arrays
                for (int i = 0; i < height; i++) {
                    energyToCurr[i] = energyToNext[i];
                    energyToNext[i] = Double.POSITIVE_INFINITY;
                }
            } else {
                // for rightmost column, there's not energy update, so we are
                // already able to find end-pixel the energy of the lowest
                // energy path and the pixel where this path ends
                for (int row = 0; row < height; row++) {
                    if (energyToCurr[row] < championEnergy) {
                        championEnergy = energyToCurr[row];
                        championRow = row;
                    }
                }
            }
        }
        // declare array where seam will be stored
        final int[] rows = new int[width];
        // declare cursor for seam rows
        int currRow = championRow;
        for (int col = width - 1; col >= 0; col--) {
            // add current row to path
            rows[col] = currRow;
            // retrieve row that led to current pixel
            currRow = rowTo[col][currRow];
        }
        return rows;
    }

    private int[] reachableCols(int col) {
        if (col == 0) {
            return new int[] { col, col + 1 };
        } else if (col == width - 1) {
            return new int[] { col - 1, col };
        } else {
            return new int[] { col -1, col, col + 1 };
        }
    }
 
    // sequence of indices for vertical seam
    public int[] findVerticalSeam() {
        // handle case where there's only one image column left
        if (width == 1) {
            final int[] cols = new int[height];
            for (int i = 0; i < height; i++) cols[i] = 0;
            return cols;
        }
        // we only have to keep two arrays in memory for iteration: one
        // containing the total energy for each pixel in the current row,
        // and another for relaxation of the following row. Also define
        // the colTo matrix to store the column visited to before getting
        // to a pixel when following the lowest energy path
        double[] energyToCurr = new double[width];
        double[] energyToNext = new double[width];
        final int[][] colTo = new int[width][height];
        // initialize the energy arrays and the first row of colTo
        for (int col = 0; col < width; col++) {
            energyToCurr[col] = 1000;
            energyToNext[col] = Double.POSITIVE_INFINITY;
            colTo[col][0] = col;
        }
        // declare variables for holding lowest energy champion and the
        // column where its last pixel is found
        double championEnergy = Double.POSITIVE_INFINITY;
        int championColumn = -1;
        // process arrays in ascending row order. Because pixels can only
        // reach other pixels below them, this performs all necessary
        // relaxations in one pass and thus has the same effect as topological
        // sorting in explicit edge-weighted graph implementations
        for (int row = 0; row < height; row++) {
            // if row is not bottomost row
            if (row < height - 1) {
                // memoize energy calculations for adjancent row
                final double[] adjEnergies= new double[width];
                for (int col = 0; col < width; col++) {
                    adjEnergies[col] = energy(col, row + 1);
                }
                for (int col = 0; col < width; col++) {
                    // get columns reachable from current pixel
                    final int[] reachableCols = reachableCols(col);
                    for (int i = 0; i < reachableCols.length; i++) {
                        // get reachable column
                        final int adjCol = reachableCols[i];
                        // relax reachable pixel in just retrieved column
                        if (energyToNext[adjCol] > energyToCurr[col] + adjEnergies[adjCol]) {
                            energyToNext[adjCol] = energyToCurr[col] + adjEnergies[adjCol];
                            colTo[adjCol][row + 1] = col;
                        }
                    }
                }
                // advance iteration arrays
                for (int i = 0; i < width; i++) {
                    energyToCurr[i] = energyToNext[i];
                    energyToNext[i] = Double.POSITIVE_INFINITY;
                }
            } else {
                // for bottomost row, there's not energy update, so we are
                // already able to find end-pixel the energy of the lowest
                // energy path and the pixel where this path ends
                for (int col = 0; col < width; col++) {
                    if (energyToCurr[col] < championEnergy) {
                        championEnergy = energyToCurr[col];
                        championColumn = col;
                    }
                }
            }
        }
        // declare array where seam will be stored
        final int[] cols = new int[height];
        // declare cursor for seam columns
        int currCol = championColumn;
        for (int row = height - 1; row >= 0; row--) {
            // add current column to path
            cols[row] = currCol;
            // retrieve column that led to current pixel
            currCol = colTo[currCol][row];
        }
        return cols;
    }

    private void validateSeam(int[] seam, String flag) {
        if (seam == null) throw new IllegalArgumentException();
        if (flag.equals("vertical")) {
            if (width <= 1) throw new IllegalArgumentException();
            if (seam.length != height) throw new IllegalArgumentException();
            validatePixel(seam[0], 0);
            for (int i = 1; i < height; i++) {
                // check whether pixel is in valid range
                validatePixel(seam[i], i);
                // check whether two adjacent entries differ by more than one
                if (Math.abs(seam[i] - seam[i-1]) > 1) throw new IllegalArgumentException();
            }
        } else {
            if (height <= 1) throw new IllegalArgumentException();
            if (seam.length != width) throw new IllegalArgumentException();
            validatePixel(0, seam[0]);
            for (int i = 1; i < width; i++) {
                validatePixel(i, seam[i]);
                if (Math.abs(seam[i] - seam[i-1]) > 1) throw new IllegalArgumentException();
            }
        }
    }
 
    // remove horizontal seam from current picture
    public void removeHorizontalSeam(int[] seam) {
        validateSeam(seam, "horizontal");
        final Picture tmp = new Picture(width, height - 1);
        // iterate over current pixels
        for (int col = 0; col < width; col++) {
            int tmpRow = 0;
            for (int row = 0; row < height; row++) {
                // keep pixel in picture as long as it is not in seam
                if (row != seam[col]) {
                    final Color color = pic.get(col, row);
                    tmp.set(col, tmpRow++, color);
                }
            }
        }
        pic = tmp;
        height = pic.height();
    }
 
    // remove vertical seam from current picture
    public void removeVerticalSeam(int[] seam) {
        validateSeam(seam, "vertical");
        // declare pixels array holding seamed picture
        final Picture tmp = new Picture(width - 1, height);
        // iterate over current pixels
        for (int row = 0; row < height; row++) {
            int tmpCol = 0;
            for (int col = 0; col < width; col++) {
                // keep pixel in picture as long as it is not in seam
                if (col != seam[row]) {
                    final Color color = pic.get(col, row);
                    tmp.set(tmpCol++, row, color);
                }
            }
        }
        pic = tmp;
        width = pic.width();
    }
 
    //  unit testing (optional)
    public static void main(String[] args) {
        final Picture picture = new Picture("https://coursera.cs.princeton.edu/algs4/assignments/seam/files/6x5.png");
        final SeamCarver sc = new SeamCarver(picture);
        assert sc.height() == 5;
        final int[] vSeam = sc.findVerticalSeam();
        System.out.print("vertical seam: {");
        for (int c : vSeam) {
            System.out.print(c + " ");
        }
        System.out.print("}\n");
        // horizontal seam
        final int[] hSeam = sc.findHorizontalSeam();
        System.out.print("horizontal seam: {");
        for (int r : hSeam) {
            System.out.print(r + " ");
        }
        System.out.print("}\n");
        sc.removeVerticalSeam(vSeam);
        sc.removeVerticalSeam(sc.findVerticalSeam());
        // // get picture after removal
        // final Picture resized = sc.picture();
        // assert resized.width() == 5;
        // assert resized.height() == 5;

        System.out.println("** TESTS RAN SUCESSFULLY **");
    }
 
 }
 